<?php
/*
Template Name: パスワードが不明な場合
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<section id="sec01">
	<div class="hero">
		<div class="pp_page_ttl_wrap">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">パスワードが不明な場合</span><span class="pp_page_ttl_en overpass">FOR MEMBERS</span></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumbs_wrap">
		<div class="contents_body_03">
			<div class="breadcrumbs submember_breadcrumbs">
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
				<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>/member/">会員専用ログイン</a></div>
				<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list">パスワードが不明な場合</div>
			</div>
		</div>
	</div>
</section>
<section id="sec02" class=" ">
	<div class="contents_body_03 unknown_wrap ">
		<div class="inner_01 submember">
			<div class="text_box">
				<ul class="blocklist">
					<li class="blockitem_01">ご不明な場合は、お手数ですが弊社までご連絡ください。</li>
					<ul class="blocklist_02">
						<li class="blockitem_02">●自動車保険課　：<br>　0120-279-841　平日　9:00～16:00</li>
						<li class="blockitem_02">●傷害医療保険課：<br>　0120-189-841　平日　9:00～16:00</li>
					</ul>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
