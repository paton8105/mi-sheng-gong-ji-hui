<?php
// ウィジェットエリア
// サイドバーのウィジェット
register_sidebar( array(
	'name' => __( 'Side Widget' ),
	'id' => 'side-widget',
	'before_widget' => '<li class="widget-container">',
	'after_widget' => '</li>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// フッターエリアのウィジェット
register_sidebar( array(
	'name' => __( 'Footer Widget' ),
	'id' => 'footer-widget',
	'before_widget' => '<div class="widget-area"><ul><li class="widget-container">',
	'after_widget' => '</li></ul></div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// アイキャッチ画像
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size(220, 165, true ); // 幅 220px、高さ 165px、切り抜きモード

// カスタムナビゲーションメニュー
add_theme_support('menus');

// // 絵文字機能の削除
// function disable_emojis() {
// 	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
// 	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
// 	remove_action( 'wp_print_styles', 'print_emoji_styles' );
// 	remove_action( 'admin_print_styles', 'print_emoji_styles' );     
// 	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
// 	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );  
// 	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
// 	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
// }
// add_action( 'init', 'disable_emojis' );
// 固定ページの親ページを判別して条件分岐
function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}

/* 投稿アーカイブページの作成 */
/*
function post_has_archive( $args, $post_type ) {

	if ( 'post' == $post_type ) {
		$args['rewrite'] = true;
		$args['has_archive'] = 'news'; //任意のスラッグ名
	}
	return $args;

}
add_filter( 'register_post_type_args', 'post_has_archive', 10, 2 );
*/
function my_archives_link($link_html){
  $my_month = '/' . get_query_var('year');

  if (strstr($link_html, $my_month)) {
    $link_html = preg_replace('@<li>@i', '<li class="current">', $link_html);
  }

  return $link_html;
}
add_filter('get_archives_link', 'my_archives_link');

// 自前ログインチェック
$member_chk;
$CHKPASS = 'yayoi0841';// ここのパスワードと照合する グローバル
$msg = '';
$pass = '';
$topchk = 'no';
$top_erjump = '';

/* 危険文字列置換ファンクション */
function Chk_StrMode($str){

		// タグを除去
		$str = strip_tags($str);
		// 空白を除去
		$str = mb_ereg_replace("^(　){0,}","",$str);
		$str = mb_ereg_replace("(　){0,}$","",$str);
		$str = trim($str);
		// 特殊文字を HTML エンティティに変換する
		$str = htmlspecialchars($str);

		return $str;
}
/* 未入力チェックファンクション */
function Chk_InputMode($str,$mes){  
		$errmes = "";
		if($str == ""){$errmes .= "{$mes}<br>\n";}
		return $errmes;
}

function mlogin_chk(){
	global $member_chk;
	if( is_page('member/insurance') || is_page('member/insurance/car') || is_page('member/insurance/group') || is_page('member/insurance/group/nursing') || is_page('member/insurance/group/medical') || is_page('member/insurance/group/retiree-injury') || is_page('member/insurance/group/injury') || is_page('member/insurance/group/retiree') ){
		// ログイン確認ページなのでログインを確認する
		if(!isset($_COOKIE['mlogin_chk'])) {
			$mloginchk_flg = 'no';
		} else {
			$mloginchk_flg = $_COOKIE["mlogin_chk"];
		}
		if( $mloginchk_flg != 'mlogin_chk_ok' ){
			$member_chk = 'true';
			// ログインしていないまたはクッキーが定義されていない
			$home_txt = home_url();
			$redirect_txt = $home_txt.'/member';
			wp_redirect($redirect_txt);
			exit;
		} else {
			// 過去にログインがされているのでログインのクッキーの最新日時の書き換え
			$member_chk = 'false';
			setcookie("mlogin_chk","mlogin_chk_ok",time()+60*60*24*7);
		}
	} elseif( is_page('member') || is_front_page() || is_home() ){
		global $msg;
		global $pass;
		global $CHKPASS;
		global $topchk;
		global $top_erjump;
		$top_erjump = '';
		$msg = '';
		$topchk = 'yes';

		/* form アクション */
		switch($_POST["action"]):
		case "loginchk":
			$msg = 'ログインチェック';
			// 不正アクセスチェック
		/*
			if(!$noindexaccess){
					header("HTTP/1.0 404 Not Found");exit();
			}
		*/


			#----------------------------------------------------------------------------------
			# データの受け取りと危険文字列置換  ※Chk_StrMode(文字列);
			#----------------------------------------------------------------------------------
			$param = array();

			// 引数を元に文字列処理及び変換処理を行う
			foreach($_POST as $k=>$e):
					$params[$k] = Chk_StrMode($e);
			endforeach;

			// 変数に入れる
			extract($params);
			if( $pass === $CHKPASS ){
				// パスワードが正しければリダイレクト
				$home_txt = home_url();
				$redirect_txt = $home_txt.'/member/insurance';
		/*
				wp_redirect($redirect_txt);
				exit;
		*/
				$msg = $redirect_txt;
				wp_redirect($redirect_txt);
				setcookie("mlogin_chk","mlogin_chk_ok",time()+60*60*24*7);
				exit;
			} elseif( $pass === 'del' ) {
				// 裏コードクッキーの削除
				setcookie("mlogin_chk","mlogin_chk_ok",time()-30);
				$msg = 'クッキーを削除しました。';
				$pass = '';
			} else {
				$msg = 'パスワードが間違っています。';
				$pass = '';
				$top_erjump = "window.location.hash = 'login';";
			}
/*
			$v = var_export($_POST);
			$topchk = 'yesac['.$_POST["action"].']['.$_POST["pass"].']';
*/

			break;

		default:
			$topchk = 'yesyes['.$_POST["action"].']';
		endswitch;
	}

}

/*
function mlogin_chk2(){
	global $member_chk;
	$member_chk = 'test2';
	echo '<!-- '.$member_chk.' -->';
}
*/

?>