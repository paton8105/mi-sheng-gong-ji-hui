</div>
<div class="foot_tel">
	<div class="foot_tel_body">
		<div class="foot_tel_content">
			<div class="foot_tel_txt">
				<p>お気軽にお問合せください。</p>
				<p>専門スタッフがご相談を承ります。</p>
			</div>
			<div class="foot_contact_bnr_wrap">
				<div class="foot_contact_bnr">
					<a href="<?php echo home_url(); ?>/contact/">お問合せ一覧を見る<img src="<?php echo get_template_directory_uri(); ?>/images/common/footer_contact_arw.svg" alt="右矢印" class="arw_icon"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<section id="footer">
	<footer>
		<div class="footer_contents_wrap">
			<div class="footer_contents">
				<div class="footer_logo"><img src="<?php echo get_template_directory_uri(); ?>/images/common/footer_logo.png" alt="株式会社弥生共済会" /></div>
				<nav class="footer_nav">
					<ul>
						<li><a href="<?php echo home_url(); ?>">トップページ</a></li>
						<li><a href="<?php echo home_url(); ?>/news/">お知らせ</a></li>
						<li><a href="<?php echo home_url(); ?>/about/">企業情報</a></li>
						<li><a href="<?php echo home_url(); ?>/access/">交通アクセス</a></li>
						<li><a href="<?php echo home_url(); ?>/faq/">よくある質問</a></li>
						<li><a href="<?php echo home_url(); ?>/contact/">お問合せ</a></li>
						<li><a href="<?php echo home_url(); ?>/privacy-policy/">個人情報保護方針</a></li>
						<li><a href="<?php echo home_url(); ?>/solicitation-policy/">勧誘方針</a></li>
						<li><a href="<?php echo home_url(); ?>/customer-first/">「お客様本位の業務運営」に関する方針</a></li>
					</ul>
				</nav>
			</div>
			<div class="copy overpass"><small>Copyright &copy; 株式会社弥生共済会 All Rights Reserved.</small></div>
		</div>
	</footer>
</section>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common.css">
<?php if (is_page('top')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/top.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css">
<?php endif; ?>
<?php if (is_page('member') || is_page('forgot')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/member.css">
<?php endif; ?>
<?php if (is_page('member/insurance')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/car.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/member_insurance.css">
<?php endif; ?>
<?php if (is_page('about')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/about.css">
<?php endif; ?>
<?php if (is_page('solicitation-policy')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/solicitation_policy.css">
<?php endif; ?>
<?php if (is_page('access')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/access.css">
<?php endif; ?>
<?php if (is_page('car')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/car.css">
<?php endif; ?>
<?php if (is_page('faq')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/faq.css">
<?php endif; ?>
<?php if (is_page('contact')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/contact.css">
<?php endif; ?>
<?php if (is_page('privacy-policy')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/privacy_policy.css">
<?php endif; ?>
<?php if (is_page('customer-first')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/customer_first.css">
<?php endif; ?>
<?php if (is_page('group-insurance')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/car.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/group-insurance.css">
<?php endif; ?>
<?php if (is_page('other')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/car.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/other.css">
<?php endif; ?>
<?php if(is_404()): ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/404.css">
<?php endif; ?>
<?php if (is_page('news-archive') || is_page('news-single') || is_page('news') || is_home() || is_archive() || is_category() || is_single($post)) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/news.css">
<?php endif; ?>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/common/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/common/apple-touch-icon-180x180.png">
<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.2.0/iscroll.min.js" async></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js" async></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.inview.min.js" async></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/ofi.min.js" async></script>
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/flexibility.js"></script>
<script>
$(function(){
	flexibility(document.documentElement);
});
</script>
<![endif]-->
<script src="<?php echo get_template_directory_uri(); ?>/js/site.js"></script>
<!-- <script src="<?php echo get_template_directory_uri(); ?>/js/nanoScroller.js"></script> -->
<script>
$(document).ready(function() {
	if (window.matchMedia( "(max-width: 1024px)" ).matches) {
		$(".nano").nanoScroller({ stop: true });
	} else {
		$(".nano").nanoScroller();
	}
});
</script>

<?php if (is_page('top')) : ?>
	<script src="<?php echo get_template_directory_uri(); ?>/js/swiper.min.js"></script>
	<script>
		var swiper = new Swiper('.swiper-container', {
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			loop: true,
			effect: "fade", //フェードのエフェクト
			speed: 1000,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
		});
	</script>
<?php endif; ?>
<?php if (is_page('faq')) : ?>
	<script>
		$(document).ready(function() {
			$('.tg_bt').addClass('close');
			$('.tg_area').hide();
		});
		$('.tg_bt').click(function() {
			$(this).toggleClass('close');
			$(this).parent().find('.tg_area').slideToggle();
		});
	</script>
<?php endif; ?>
<?php if (is_page('contact')) : ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/sc_modal.css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/sc_modal.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/table_chg.js"></script>


<script>
//	$(document).ready(function() {
//		$(".nano2").nanoScroller({ contentClass: 'nano2_con' });
//	});
</script>
<?php endif; ?>
<?php if (is_page('top')) : ?>
	<script>
<?php
		global $top_erjump;
		global $msg;
?>
<?php echo $top_erjump ?>
<?php echo '<!--' ?>
<?php echo $msg ?>
<?php echo '-->' ?>
	</script>

<?php endif; ?>

<link href="https://fonts.googleapis.com/css2?family=Overpass:wght@300;400;700&display=swap" rel="stylesheet">
<?php wp_footer(); ?>
</div>
</div>
</body>

</html>