<?php
/*
Template Name: ホームページをオープンしました
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<main class="news">
	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<div class="breadcrumbs_wrap">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>/news-archive/">お知らせ一覧</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">ホームページをオープンしました</div>
					</div>
				</div>
			</article>
		</div>
	</section>
	<section id="sec_content01">
		<div class="contents_body_01">
			<div class="news_single_wrap">
				<article class="article_list">
					<div class="single_ttlbox">
						<div class="sigle_post_date overpass">2020.00.00</div>
						<div class="sigle_post_cat">お知らせ</div>
						<div class="sigle_post_ttl">ホームページをオープンしました</div>
					</div>

					<div class="single_content">
						<p>株式会社弥生共済会のホームページをご利用いただきありがとうございます。<br>
							使いやすく、わかりやすいホームページになりますよう、内容の充実を図るとともに、様々な情報を発信してまいります。<br>
							どうぞよろしくお願いいたします。</p>
						<br><br>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/images/news/mockup.png">
						</figure>
					</div>
					<div class="single_link">
						<div class="post_prev"> <a href="" rel="prev">Prev</a></div>
						<div class="back_btn"><a href="">一覧へ</a></div>
						<div class="post_next"> <a href="" rel="next">Next</a></div>
					</div>

				</article>
				<aside class="archive_list">
					<div class="archive_listbg">
						<div class="archive_wrap">
							<h2 class="archive_listttl overpass">ARCHIVES</h2>
							<ul class="archive_listbox">
								<li><a class="archive_item active overpass" href="">2020</a></li>
								<li><a class="archive_item overpass" href="">2019</a></li>
								<li><a class="archive_item overpass" href="">2018</a></li>
							</ul>
						</div>
						<div class="category_wrap">
							<h2 class="archive_listttl">CATEGORY</h2>
							<ul class="cat_list">
								<li><a class="cat_item" href=""><span class="cat_name">お知らせ</span><span class="cal_items overpass">18</span></a></li>
								<li><a class="cat_item" href=""><span class="cat_name">キャンペーン</span><span class="cal_items overpass">4</span></a></li>
							</ul>
						</div>
					</div>
				</aside>

			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>