	
var $window = $(window),
    $html = $('html'),
    $body = $('body'),
    $overlay = $('.overlay'),
    scrollbar_width = window.innerWidth - document.body.scrollWidth,
    touch_start_y;

// タッチしたとき開始位置を保存しておく
$window.on('touchstart', function(event) {
  touch_start_y = event.originalEvent.changedTouches[0].screenY;
});

console.log('modal');


// つぶやきをクリック
$('.moadlbt').on('click', function() {
  // スワイプしているとき
  $window.on('touchmove.noscroll', function(event) {
    var overlay = $overlay[0],
        current_y = event.originalEvent.changedTouches[0].screenY,
        height = $overlay.outerHeight(),
        is_top = touch_start_y <= current_y && overlay.scrollTop === 0,
        is_bottom = touch_start_y >= current_y && overlay.scrollHeight - overlay.scrollTop === height;
    
    // スクロール対応モーダルの上端または下端のとき
    if (is_top || is_bottom) {
      // スクロール禁止
      event.preventDefault();
    }
  });
  
  $('html, body').css('overflow', 'hidden');
  
  if (scrollbar_width) {
    $html.css('padding-right', scrollbar_width);
  }
	
	var set_classname;
	set_classname = $(this).data('modalconname');
	var set_html = $( '.'+set_classname ).html();
	$('.modal').html(set_html);
  
  // モーダルをフェードイン
  $overlay.fadeIn(300);
	// ここでスクロールバーをつける
/*
	$(".nano2").nanoScroller({ contentClass: 'nano2_con' });
*/
});

// モーダルを閉じる処理
var closeModal = function() {
  // overflow: hidden; と padding-right を消す
  $body.removeAttr('style');
  
  // イベントを削除
  $window.off('touchmove.noscroll');
  
  // モーダルをフェードアウト
  $overlay.animate({
    opacity: 0
  }, 300, function() {
    // モーダルを一番上にスクロールしておく
    $overlay.scrollTop(0).hide().removeAttr('style');
    // overflow: hidden を消す
    $html.removeAttr('style');
  });
};

// オーバーレイをクリック
$overlay.on('click', function(event) {
  // モーダルの領域外をクリックで閉じる
  if (!$(event.target).closest('.modal').length) {
    closeModal();
  }
});

// 閉じるボタンクリックでモーダルを閉じる
$('.modal_closeicon').on('click', function() {
  closeModal();
});

	
