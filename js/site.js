jQuery(function($){ //
	
window.onload = function() {
  let spinner = document.getElementById('my-spinner');
  // .box
  spinner.classList.add('loaded');
}
	
$(window).load(function() {
	$("body").removeClass("preload");
});
//ナビ
$('.nav_btn').on('click',function(){
	$('.nav_btn_line').toggleClass('active');
	$('.gnav').fadeToggle();
});
$(function(){
  var flg = "default";
  $('.nav_btn').click(function(){
    if(flg == "default"){
      $(".nav_btn_txt").text("閉じる");
			$(".nav_btn_txt").addClass("close");
      flg = "changed";
    }else{
      $(".nav_btn_txt").text("メニュー");
			$(".nav_btn_txt").removeClass("close");
      flg = "default";
    }
  });
});
$(document).ready(function() {
	if (window.matchMedia( "(max-width: 1024px)" ).matches) {
		$(function(){
			$(".nano-pane").css("display","none");
		});
	} else {
		$(function(){
			var _scroll = "default";
			$('.nav_btn').click(function(){
				if(_scroll == "default"){
					$(".nano-pane").css("display","none");
					_scroll = "changed";
				}else{
					$(".nano-pane").css("display","block");
					_scroll = "default";
				}
			});
		});
	}
});
//ページトップ
$(document).ready(function() {
	$(".page_top").hide();
	$('.nano-content').scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.page_top').fadeIn("fast");
		} else {
			$('.page_top').fadeOut("fast");
		}
	});
});
$('.nano-content').scroll(function() {
	scrollHeight = $('.wrap').innerHeight() + $('.foot_tel').innerHeight();
	scrollPosition = $('.nano-content').height() + $('.nano-content').scrollTop();
	footHeight = $("footer").innerHeight();
console.log('scrollHeight='+scrollHeight);
console.log('nano-content='+$('.nano-content').height() );
console.log('scrollPosition='+scrollPosition);
console.log('footHeight='+footHeight);
	page_toph = $(".page_top").innerHeight();
console.log('page_toph='+page_toph);
	if ( ( scrollHeight + 30 + ( page_toph * 0.5 ) ) - scrollPosition <= 0 ) {
/*
		$(".page_top").css({
			"position": "absolute",
			"bottom": footHeight - 50
		});
*/
		var set_ftpx = ( scrollPosition - scrollHeight ) - ( page_toph * 0.5 ) ;
console.log('set_ftpx='+set_ftpx);
		$(".page_top").css({
			"position": "fixed",
			"bottom": set_ftpx +"px"
		});
	} else {
		$(".page_top").css({
			"position": "fixed",
			"bottom": "30px"
		});
	}
});
$('.page_top a').click(function() {
	$('.nano-content').animate({
		scrollTop: 0
	}, 500);
	return false;
});

//画像切り替え
$(window).on('load resize', function(){
	var $elem = $('.image-switch');
	var sp = '_sp.';
	var pc = '_pc.';
	var replaceWidth = 640;
	function imageSwitch() {
		var windowWidth = parseInt($(window).width());
		$elem.each(function() {
			var $this = $(this);
			if (windowWidth >= replaceWidth) {
				$this.attr('src', $this.attr('src').replace(sp, pc));
			} else {
				$this.attr('src', $this.attr('src').replace(pc, sp));
			}
		});
	}
	imageSwitch();
	var resizeTimer;
	$(window).on('resize', function() {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			imageSwitch();
		}, 50);
	});
});

//ページ内スクロール
$(function(){
	$('a[href^="#"]').click(function() {
	var speed = 400; // スクロールスピード(ミリ秒)
	var href= $(this).attr("href");
	var target = $(href == "#" || href == "" ? 'html' : href);
	var position = target.offset().top;
	$('body,html').animate({scrollTop:position}, speed, 'swing');
	return false;
	});
});

// スライド画像切り替え
	$(function () {
		// 置換の対象とするclass属性。
		var $elem_slide = $('.slide-change');
		// 置換の対象とするsrc属性の末尾の文字列。
		var sp = '_sp.';
		var tb = '_tb.';
		var pc = '_pc.';
		// 画像を切り替えるウィンドウサイズ。
		var replaceWidth01 = 641;
		var replaceWidth02 = 769;
		function imageSwitch() {
			var windowWidth = parseInt($(window).width());
			$elem_slide.each(function () {
				var $this = $(this);
				if (windowWidth >= replaceWidth01) {
					$this.attr('src', $this.attr('src').replace(sp, tb));
				} else {
					$this.attr('src', $this.attr('src').replace(tb, sp));
				}
			});
			$elem_slide.each(function () {
				var $this = $(this);
				if (windowWidth >= replaceWidth02) {
					$this.attr('src', $this.attr('src').replace(tb, pc));
				} else {
					$this.attr('src', $this.attr('src').replace(pc, tb));
				}
			});
		}
		imageSwitch();
		// 動的なリサイズは操作後0.2秒経ってから処理を実行する。
		var resizeTimer;
		$(window).on('resize', function () {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function () {
				imageSwitch();
			}, 50);
		});
	});
	
	//トップナビフローティング
	$(document).ready(function() {
		$('.nano-content').scroll(function() {
			if ($(this).scrollTop() > 400 ) {
				$('.top_floting').addClass('top_floting_show');
			} else {
				$('.top_floting').removeClass('top_floting_show');
			}
		});
	});

	
}); //
