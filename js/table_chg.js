
// テーブルをスマホサイト用に変更する。もう少し汎用性のあるものに
// 

const FLG_RESET = -1;

// セルの情報
function TCell( _val, _len ){
	this.val = _val;
	this.len = _len;
	this.flg = false;
}

// 面倒そうなのでテーブルデータ多重配列を取り扱うクラス
function TDat( _x, _y ){
	this.x = _x;
	this.y = _y;
	this.dat; // 多重配列[縦][横]
	this.dat = Array(_y);
	for( var e=0; e<_y; e++ ){
		this.dat[e] = Array(_x);
	}
	for( e=0; e<_y; e++ ){
		for( var n=0; n<_x; n++ ){
			this.dat[e][n] = new TCell( '', 0 );
		}
	}
	// データを設定する関数
	// _xx 配列横
	// _yy 配列縦
	// _ww 配列横の繰り返し値(colspan)
	// _hh 配列縦の切り返し値(rowspan)
	// 設定する値
	this.setdat = function( _xx, _yy, _ww, _hh, _val ){
		var max_x = 0;
		var max_y = 0;
		max_x = _xx + _ww;
		max_y = _yy + _hh;
		for( var e=_yy; e<max_y; e++ ){
			for( var n=_xx; n<max_x; n++ ){
				this.dat[e][n].val = _val;
				this.dat[e][n].flg = true;
			}
		}
	}
	
}

function TableCreate( _doc ){
	this.table_th = [];
	this.table_td;
	this.doc = _doc;

	// テーブルのヘッダーを検索する
	// テーブルのヘッダーからテーブルの横の長さを取得

	// テーブルのtrをけんせくして最大行数を取得
	// テーブルの縦横のデータを作成する
	// tr td に関して行ごとのデータを取り出す
	// そのさいデータを埋めてゆく colspan rowspan を判断しテーブルを埋めてゆく
	// の処理をしてデータを格納しておく
	var self = this;
	var maxarys_w = 0;
	$(_doc).find('th').each( function( i ){
		var setattr = $(this).attr('colspan');
		var setcol = 1;
		if( setattr != undefined ){
			setcol = Number(setattr);
		}
		maxarys_w += setcol;
		self.table_th[i] = new TCell( $(this).html(), setcol );
	});
	// まずデータを作成する
	var maxarys_h = $(_doc).find('tr').length;
	maxarys_h -= 1;
	this.table_td = new TDat(maxarys_w,maxarys_h);

	// 行ごとの処理で結合されているデータを全て個別のセルのイメージデータを並べる
	$(_doc).find('tr').each( function( i ){
		if( i == 0 ){
			// thは１行目のみ決め打ちcontinue
			return true;
		}
		var yy = i-1;
		// データを収納するためにtdを検出する
		var jx = 0;
		for( var cx=0; cx<maxarys_w; cx++ ){
			if( self.table_td.dat[yy][cx].flg == false ){
				var set_col = 1;
				var set_row = 1;
				var set_val = '';
				$(this).find('td').each( function( index ){
					var set_index = index + jx;
					if( set_index == cx ){
						set_col = $(this).attr('colspan');
						set_row = $(this).attr('rowspan');
						set_val = $(this).html();
						if( set_col == undefined){
							set_col = 1;
						}
						if( set_row == undefined){
							set_row = 1;
						}
						self.table_td.setdat( cx, yy, Number(set_col), Number(set_row), set_val );
					}
				});
			} else {
				jx++;
			}
			
		}
		
	});

	// テーブルの出力
	// tr１つで１データとしてデータを埋めてゆく
	// thで横結合の場合
	// th
	// td
	// td
	// となる。
	// 別の出力パターンの場合また別の関数を作るべき
	this.createsp_tb = function(){
		var out = '';
		var max_tr = this.table_td.y;
		// 行
		for( var e=0; e<max_tr; e++ ){
			var max_th = this.table_th.length;
			var pos_td = 0;
			out += '<section class="sp_table">';
			// TH（タイトル）の個数
			for( var n=0; n<max_th; n++){
				out += '<div class="spth">'+this.table_th[n].val+'</div>\n';
				var max_thcol = this.table_th[n].len;
				// THが内包している最終的なTDの数ぶんをループ
				for( var ctd=0; ctd<max_thcol; ctd++ ){
					var outval = self.table_td.dat[e][pos_td].val;
					if( ctd == 0 ){
						out += '<div class="sptd">'+outval+'</div>\n';
					} else {
						out += '<div class="sptd btop">'+outval+'</div>\n';
					}
					pos_td++;
				}
			}
			out += '</section>';
		}
		var out_doc = this.doc;
		var tb_sp_find = $(out_doc).parent().find('.tb_sp');
		tb_sp_find.html(out);
		
	}

}

//console.log('tb jikkou2');
// テーブルを可変させてスマホ用のテーブルを作り出す
$(window).on("load", function(){
//console.log('tb jikkou');
	// テーブルごとに定義して作成する
	var tb = new TableCreate('.modal_con01 .chg_tb');
	tb.createsp_tb();
	var tb2 = new TableCreate('.modal_con02 .chg_tb');
	tb2.createsp_tb();
	var tb3 = new TableCreate('.modal_con03 .chg_tb');
	tb3.createsp_tb();
	var tb4 = new TableCreate('.modal_con04 .chg_tb');
	tb4.createsp_tb();
	var tb5 = new TableCreate('.modal_con05 .chg_tb');
	tb5.createsp_tb();
});



