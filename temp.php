<?php
/*
Template Name: 固定ページテンプレート
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<section id="sec01">
  <div class="hero">
    <div class="page_body_01">
      <img src="<?php echo get_template_directory_uri(); ?>/images/temp/hero_pc.jpg" alt="交通アクセス" class="image-switch">
      <div class="page_ttl">
        <h1><span class="page_ttl_ja">交通アクセス</span><span class="page_ttl_en overpass">ACCESS</span></h1>
      </div>
    </div>
  </div>
  <div class="breadcrumbs_wrap">
    <div class="page_body_01">
      <div class="breadcrumbs">
        <div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
        <div class="breadcrumbs_list arw_breadcrumbs">〉</div>
        <div class="breadcrumbs_list">交通アクセス</div>
      </div>
    </div>
  </div>
</section>
<section id="sec02">

</section>
<section id="sec03">

</section>
<section id="sec04">

</section>
<?php get_footer(); ?>