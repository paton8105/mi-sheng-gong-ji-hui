<?php
/*
Template Name: よくある質問
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<main class="about">
	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero">
<!--
					<img src="<?php echo get_template_directory_uri(); ?>/images/faq/hero_pc.jpg" class="image-switch" alt="よくある質問">
-->
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">よくある質問</h1>
						<p class="hed_ttlen overpass">FAQ</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap_02">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">よくある質問</div>
					</div>
				</div>
			</article>
		</div>
	</section>

	<section id="sec_content02">
		<div class="contents_body_03">
			<article class="faqwrap">
				<ul class="faqbox">
					<?php

						$tabary_name = 'faq_ary';
						while(the_repeater_field($tabary_name)){
							$faq_q = get_sub_field('question');
							$faq_a = get_sub_field('answer');
					?>
					<li class="faq_item">
						
						<h2 class="tg_bt faqbt"><span class="faq_q overpass">Q</span><span class="faq_q_txt"><?php echo $faq_q ?></span><i></i></h2>
						<div class="tg_area ">
							<div class="faqans"> <span class="faq_a overpass">A</span>
								<div class="faq_a_txt">
									<p><?php echo $faq_a ?></p>
								</div>
							</div>
						</div>
					</li>
					<?php } ?>
				</ul>
			</article>
		</div>
	</section>
</main>
<?php get_footer(); ?>