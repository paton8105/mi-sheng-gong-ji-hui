<?php
/*
Template Name: 個人情報保護方針
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<section id="sec01">
	<div class="hero">
		<div class="pp_page_ttl_wrap">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">個人情報保護方針</span><span class="pp_page_ttl_en overpass">PRIVACY POLICY</span></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumbs_wrap">
		<div class="contents_body_03">
			<div class="breadcrumbs submember_breadcrumbs">
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
				<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list">個人情報保護方針</div>
			</div>
		</div>
	</div>
</section>
<section id="sec02">
	<div class="contents_body_03">
		<div class="inner_01">
			<div class="pp_wrap">
				<ul class="pp_subtxt">
					<li>
						<p>
							当社は、個人情報保護の重要性に鑑み、また保険業に対するお客さまの信頼をより向上させるため、個人情報の保護に関する法律（個人情報保護法）、行政手続における特定の個人を識別するための番号の利用等に関する法律（番号法）、その他の関係法令、関係官庁からのガイドライン、特定個人情報の適正な取扱いに関するガイドラインなどを遵守して、個人情報を厳正・適切に取り扱うとともに、安全管理について適切な措置を講じます。
						</p>
					</li>
					<li>
						当社は、個人情報の取扱いが適正に行われるように、従業者への教育・指導を徹底し、適正な取扱いが行われるよう取り組んでまいります。また、個人情報の取扱いに関する苦情・ご相談に迅速に対応し、当社の個人情報の取扱いおよび安全管理に係る適切な措置については、適宜見直し、改善いたします。
					</li>
				</ul>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						1.個人情報の取得・利用
					</h2>
					<p>当社は、業務上必要な範囲内で、かつ、適法で公正な手段により個人情報を取得・利用します。<br>（下記6. の個人番号および特定個人情報を除きます。）
					</p>
				</div>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						2.個人情報の利用目的
					</h2>
					<p>
						当社は、保険会社から保険募集業務の委託をうけて、取得した個人情報（個人番号および特定個人情報については、下記6. をご覧ください。）を当該業務の遂行に必要な範囲内で利用します。
					</p>
					<p>
						当社は複数の保険会社と取引があり、取得した個人情報を取引のある保険会社の商品・サービスをご提案するために利用させていただくことがあります。<br>
						当社における具体的な個人情報の利用目的は次のとおりであり、それら以外の他の目的に利用することはありません。
					</p>
					<p class="pp_box_02_sub_ttl">当社が取り扱う損害保険およびこれらに付帯・関連するサービスの提供</p>
					<p>
						上記の利用目的の変更は、相当の関連性を有すると合理的に認められている範囲にて行い、変更する場合には、その内容をご本人に対し、原則として書面（電磁的記録を含む。以下同じ。）などにより通知し、または当社のホームページなどにより公表します。<br>当社に対し保険業務の委託を行う保険会社の利用目的は、保険会社のホームページ（下記）に記載してあります。
					</p>
					<p>
					<ul class="hp_list_wrap">
						<li>
							<span>■ 三井住友海上火災保険株式会社</span><span>(https://www.ms-ins.com/)</span>
						</li>
						<li>
							<span>■ 日新火災海上保険株式会社</span><span>(https://www.nisshinnfire.co.jp/)</span>
						</li>
						<li>
							<span>■ 損害保険ジャパン株式会社</span><span>(https://www.sompo-japan.co,jp/)</span>
						</li>
						<li>
							<span>■ 東京海上日動火災保険株式会社</span><span>(https://www.tokiomarine-nichido.co.jp/)</span>
						</li>
						<li>
							<span>■ あいおいニッセイ同和損害保険株式会社</span><span>(https://www.aioinissaydowa.co.jp/)</span>
						</li>
					</ul>
					</p>
				</div>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						3.個人データの安全管理措置
					</h2>
					<p>
						当社は、取り扱う個人データ（下記6.の個人番号および特定個人情報を含みます。）の漏えい、滅失または毀損の防止、その他個人データの安全管理のため、安全管理に関する取扱規程などの整備および実施体制の整備など、十分なセキュリティ対策を講じるとともに、利用目的の達成に必要とされる正確性・最新性を確保するための適切な措置を講じ、万が一、問題等が発生した場合は、速やかに適当な是正対策を行います。
					</p>
					<p>
						当社は、個人データの安全管理措置に関する社内規程を別途定めており、その具体的内容は主として以下のとおりです。安全管理措置に関するご質問については、下記8. のお問い合わせ窓口までお寄せください。
					</p>
					<dl>
						<dt>(1) 基本方針の整備</dt>
						<dd>
							個人データの適正な取扱いの確保のため、「関係法令・ガイドライン等の遵守」、「安全管理措置に関する事項」、「質問および苦情処理の窓口」等について本基本方針を策定し、必要に応じて見直しています。
						</dd>

						<dt>(2) 個人データの安全管理に係る取扱規程の整備</dt>
						<dd>
							取得、利用、保存、提供、削除・廃棄等の段階ごとに、取扱方法、責任者・担当者およびその任務等についての規程を整備し、必要に応じて見直しています。
						</dd>

						<dt>(3) 組織的安全管理措置</dt>
						<dd>・個人データの管理責任者等の設置</dd>
						<dd>・就業規則等における安全管理措置の整備</dd>
						<dd>・個人データの安全管理に係る取扱規程に従った運用</dd>
						<dd>・個人データの取扱状況を確認できる手段の整備</dd>
						<dd>・個人データの取扱状況の点検及び監査体制の整備と実施</dd>
						<dd>・漏えい等事案に対応する体制の整備</dd>

						<dt>(4) 人的安全管理措置</dt>
						<dd>・従業者との個人データの非開示契約等の締結</dd>
						<dd>・従業者の役割・責任等の明確化</dd>
						<dd>・従業者への安全管理措置の周知徹底、教育及び訓練</dd>
						<dd>・従業者による個人データ管理手続の遵守状況の確認</dd>

						<dt>(5) 物理的安全管理措置</dt>
						<dd>・個人データの取扱区域等の管理</dd>
						<dd>・機器及び電子媒体等の盗難等の防止</dd>
						<dd>・電子媒体等を持ち運ぶ場合の漏えい等の防止</dd>
						<dd>・個人データの削除及び機器、電子媒体等の廃棄</dd>

						<dt>(6) 技術的安全管理措置</dt>
						<dd>・個人データの利用者の識別及び認証</dd>
						<dd>・個人データの管理区分の設定及びアクセス制御</dd>
						<dd>・個人データへのアクセス権限の管理</dd>
						<dd>・個人データの漏えい・毀損等防止策</dd>
						<dd>・個人データへのアクセスの記録及び分析</dd>
						<dd>・個人データを取り扱う情報システムの稼動状況の記録及び分析</dd>
						<dd>・個人データを取り扱う情報システムの監視及び監査</dd>

						<dt>(7) 委託先の監督</dt>
						<dd>個人データの取扱いを委託する場合には、個人データを適正に取り扱っている者を選定し、委託先における安全管理措置の実施を確保するため、外部委託に係る取扱規程を整備し、定期的に見直しています。</dd>

						<dt>(8) 外的環境の把握</dt>
						<dd>個人データを取り扱う国における個人情報の保護に関する制度を把握した上で安全管理措置を実施しています。</dd>
					</dl>
				</div>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						4.個人データの第三者への提供および第三者からの取得
					</h2>
					<p>(1) 当社は、次の場合を除き、あらかじめご本人の同意なく第三者に個人データ（個人番号および特定個人情報については、下記6. をご覧ください。）を提供しません。</p>
					<p>
					<ul class="list_number_wrap">
						<li><span class="list_number">&#9312;</span><span class="list_number_txt">法令に基づく場合</span></li>
						<li><span class="list_number">&#9313;</span><span
								class="list_number_txt">人の生命、身体又は財産の保護のために必要がある場合であって、本人の同意を得ることが困難であるとき</span></li>
						<li><span class="list_number">&#9314;</span><span
								class="list_number_txt">公衆衛生の向上又は児童の健全な育成の推進のために特に必要がある場合であって、本人の同意を得ることが困難であるとき</span></li>
						<li><span class="list_number">&#9315;</span><span
								class="list_number_txt">国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合であって、本人の同意を得ることにより当該事務の遂行に支障を及ぼすおそれがあるとき</span>
						</li>
						<li><span class="list_number">&#9316;</span><span
								class="list_number_txt">当該第三者が学術研究機関等である場合であって、当該第三者が当該個人データを学術研究目的で取り扱う必要があるとき（当該個人データを取り扱う目的の一部が学術研究目的である場合を含み、個人の権利利益を不当に侵害するおそれがある場合を除く）</span>
						</li>
					</ul>
					</p>
					<p>
						(2)
						個人データを第三者に提供したとき、あるいは第三者から取得したとき（個人関連情報を個人データとして取得する場合を含みます。）、提供・取得経緯等の確認を行うとともに、提供先・提供者の氏名等、法令で定める事項を記録し、保管します。
					</p>
				</div>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						5.センシティブ情報の取扱い
					</h2>
					<p>
						当社は、要配慮個人情報（人種、信条、社会的身分、病歴、前科・前歴、犯罪被害情報などをいいます）ならびに労働組合への加盟、門地および本籍地、保健医療および性生活に関する個人情報（センシティブ情報）については、次の場合を除き、原則として取得、利用または第三者提供を行いません。
					</p>
					<p>
					<ul class="list_number_wrap">
						<li><span class="list_number">&#9312;</span><span class="list_number_txt">法令に基づく場合</span></li>
						<li><span class="list_number">&#9313;</span><span class="list_number_txt">人の生命、身体又は財産の保護のために必要がある場合</span>
						</li>
						<li><span class="list_number">&#9314;</span><span
								class="list_number_txt">公衆衛生の向上又は児童の健全な育成の推進のため特に必要がある場合</span></li>
						<li><span class="list_number">&#9315;</span><span
								class="list_number_txt">国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合</span>
						</li>
						<li><span class="list_number">&#9316;</span><span
								class="list_number_txt">保険料収納事務等の遂行上必要な範囲において、政治・宗教等の団体若しくは労働組合への所属若しくは加盟に関する従業員等のセンシティブ情報を取得、利用又は第三者提供する場合</span>
						</li>
						<li><span class="list_number">&#9317;</span><span
								class="list_number_txt">相続手続を伴う保険金支払事務等の遂行に必要な限りにおいて、センシティブ情報を取得、利用又は第三者提供する場合</span>
						</li>
						<li><span class="list_number">&#9318;</span><span
								class="list_number_txt">保険業の適切な業務運営を確保する必要性から、本人の同意に基づき業務遂行上必要な範囲でセンシティブ情報を取得、利用又は第三者提供する場合</span>
						</li>
					</ul>
					</p>
				</div>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						6.個人番号および特定個人情報の取扱い
					</h2>
					<p>
						当社は、個人番号および特定個人情報について、法令で限定的に明記された目的以外のために取得・利用しません。番号法で限定的に明示された場合を除き、個人番号および特定個人情報を第三者に提供しません。
					</p>
				</div>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						7.個人情報保護法に基づき保有個人データの開示、訂正、利用停止など
					</h2>
					<p>
						個人情報保護法に基づく保有個人データ（上記6.
						の個人番号および特定個人情報を含みます。）に関する開示（確認・記録の開示を含む）、訂正または利用停止などに関するご請求については、データの保有者である保険会社に対してお取次ぎいたします。
					</p>
				</div>
				<div class="pp_box">
					<h2 class="pp_box_ttl">
						8.お問い合わせ先
					</h2>
					<p>ご連絡先は下記のお問い合わせ窓口となります。また保険事故に関する照会については、下記お問い合わせ窓口のほか、保険証券記載の保険会社の事故相談窓口にもお問い合わせいただくことができます。<br>
						なお、ご照会者がご本人であることをご確認させていただいたうえで、ご対応させていただきますので、あらかじめご了承願います。</p>
					<dl class="pp_box_08_about">
						<dt>＜代理店名＞</dt>
						<dd>株式会社 弥生共済会</dd>
						<dt>＜所在地＞</dt>
						<dd>東京都新宿区市谷左内町29-4</dd>
						<dt>＜代表者氏名＞</dt>
						<dd>代表取締役社長　古澤宣孝</dd>
						<dt>＜電話番号＞</dt>
						<dd>自動車保険課　0120-279-841<br>
							傷害医療保険課　0120-189-841</dd>
						<dt>＜ホームページ＞</dt>
						<dd>https://www.yayoi-kyosaikai.co.jp/</dd>
					</dl>
					<p class="pp_box_08_last_txt">＜アクセス解析ツールについて＞</p>
					<ul class="list_number_wrap">
						<li>
							<span class="list_number">&#9312;</span>
							<div class="list_number_txt">
								<ul>
									<li>
										当サイトでは、サイトの計測、利用状況の分析、サイトの改善といった目的のためにGoogleによるアクセス解析ツール「Googleアナリティクス」を利用しています。Googleアナリティクスは、「Cookie」を利用してアクセス情報を収集します。アクセス情報は匿名であり、個人を特定するものではありません。
									</li>
									<li>「Googleアナリティクス」の利用規約については<a href="https://marketingplatform.google.com/about/analytics/terms/jp/"
											class="pp_list_link" target="_blank" rel="nofollow">こちら</a>をご覧ください。</li>
								</ul>
							</div>
						</li>
						<li><span class="list_number">&#9313;</span><span
								class="list_number_txt">利用者は、当サイトを利用することでGoogleのデータ処理について許可を与えたものとみなします。</span></li>
						<li><span class="list_number">&#9314;</span><span
								class="list_number_txt">Cookieはお使いのブラウザの設定により無効にすることができます。</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="sec03">

</section>
<section id="sec04">

</section>
<?php get_footer(); ?>