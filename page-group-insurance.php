<?php
/*
Template Name: 団体傷害・医療保険
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<main class="group_insurance">

	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero">
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">団体傷害・医療保険</h1>
						<p class="hed_ttlen overpass">INSURANCE</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">団体傷害・医療保険</div>
					</div>
				</div>
			</article>
		</div>

	</section>

	<section id="sec_content01">
		<div class="contents_body_03">
			<article class="conhed_wrap">
				<figure class="conhed_bg"></figure>
				<div class="conhed_box">
					<h2 class="conhed_ttl">傷害医療保険課</h2>
					<p class="conhed_tel overpass">TEL 0120-189-841</p>
					<p class="conhed_time">受付時間　9:00～16:00</p>
					<p class="conhed_txt">自警会会員およびご家族の皆さまだけにご用意した保険制度です。日常生活の様々なリスクからご家族の皆さまをお守りします。詳細は自警会会員専用ページへログインしてください。</p>
					<a class="combtn" href="<?php echo home_url(); ?>/group-insurance/member/" >会員専用ページで詳しく見る<img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_r_wh.svg" class="arw_icon"></a>
				</div>

			</article>
		</div>
	</section>




</main>



<?php get_footer(); ?>