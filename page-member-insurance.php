<?php
/*
Template Name: 取扱保険
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<main class="group_insurance">

	<section id="sec_content01">
		<div class="menber_band">自警会会員専用ページ</div>
		<div class="hero submember_bg">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">取扱保険</span><span class="page_ttl_en overpass">INSURANCE</span></h1>
				</div>
			</div>
		</div>
		<div class="breadcrumbs_wrap">
			<div class="contents_body_03">
				<div class="breadcrumbs submember_breadcrumbs">
					<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list">取扱保険</div>
				</div>
			</div>
		</div>
		<div id="sec02">
			<div class="contents_body_03">
				<div class="insurance_bg_wrap">
					<div class="insurance_bg">
						<div class="contents_body_05">
							<div class="inner_insurancetype">
								<div class="insurancetype_list_wrap">
									<ul class="insurancetype_list">
										<li class="insurancetype_listitem">
											<a href="<?php echo home_url(); ?>/member/insurance/car/" class="insurancetype_card">
												<figure class="insurancetype_img">
													<img class="image-switch" src="<?php echo get_template_directory_uri(); ?>/images/top/img01_pc.jpg" alt="自警会団体傷害保険">
												</figure>
												<div class="insurancetype_txtbox">
													<p>自動車保険</p>
													<img src="<?php echo get_template_directory_uri(); ?>/images/top/arw_r.svg" alt="右矢印" class="arw_r">
												</div>
											</a>
										</li>
										<li class="insurancetype_listitem">
											<a href="<?php echo home_url(); ?>/member/insurance/group/" class="insurancetype_card">
												<figure class="insurancetype_img">
													<img class="image-switch" src="<?php echo get_template_directory_uri(); ?>/images/top/img02_pc.jpg" alt="自警会団体傷害保険、退職者延長保険">
												</figure>
												<div class="insurancetype_txtbox">
													<p>団体傷害・医療保険</p>
													<img src="<?php echo get_template_directory_uri(); ?>/images/top/arw_r.svg" alt="右矢印" class="arw_r">
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="insurancetype_attention">
								<p>このホームページは、各保険の概要についてご紹介しており、特定の保険会社名や商品名のない記載は一般的な保険商品に関する説明です。</p>
								<p>取扱商品、各保険の名称や補償内容は引受保険会社によって異なりますので、ご加入にあたっては、必ず重要事項説明書や各保険のパンフレット等をよくお読みください。</p>
								<p>ご不明点等がある場合には、代理店までお問合わせください。</p>
						</div>
				</div>
			</div>
		</div>
	</section>
</main>



<?php get_footer(); ?>