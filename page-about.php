<?php
/*
Template Name: 企業情報
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<main class="about">

	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero">
					<!--
					<img src="<?php echo get_template_directory_uri(); ?>/images/about/hero_pc.jpg" class="image-switch" alt="企業情報">
-->
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">企業情報</h1>
						<p class="hed_ttlen overpass">ABOUT US</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap_02">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">企業情報</div>
					</div>
				</div>
			</article>
		</div>

	</section>

	<!--
	<section id="sec_content01" >
		<div class="contents_body_02">
			<article class="conhed_wrap">
				<figure class="conhed_bg"></figure>
				<div class="conhed_box">
					<h2 class="conhed_ttl">経営理念</h2>
					<div class="s_underbar"></div>
					<h3 class="conhed_philosophy">ここに経営理念が入ります。<br>ここに経営理念が入ります。</h3>
					<p class="conhed_txt">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
				</div>

			</article>
		</div>
	</section>	
-->

	<section id="sec_content02">
		<div class="contents_body_03">
			<article class="about_wrap">

				<div class="about_conbox">
					<aside class="about_ttlbox">
						<h2 class="about_ttl">会社概要</h2>
						<div class="underbar"></div>
					</aside>
					<article class="about_infobox">
						<dl class="about_dl">
							<dt class="about_dt">商号</dt>
							<dd class="about_dd">株式会社 弥生共済会</dd>
							<dt class="about_dt">代表者</dt>
							<dd class="about_dd">代表取締役　古澤 宣孝</dd>
							<dt class="about_dt">所在地</dt>
							<dd class="about_dd">東京都新宿区市谷左内町29番地</dd>
							<dt class="about_dt">設立</dt>
							<dd class="about_dd">昭和50年4月</dd>
							<dt class="about_dt">資本金</dt>
							<dd class="about_dd">1000万円</dd>
							<dt class="about_dt">営業種別</dt>
							<dd class="about_dd">損害保険代理店</dd>
							<dt class="about_dt">従業員数</dt>
							<dd class="about_dd">46人（2024年1月1日現在）</dd>
							<dt class="about_dt">取引保険会社<br>（50音順）</dt>
							<dd class="about_dd">あいおいニッセイ同和損害保険株式会社　<br><span class="ilbk">損害保険ジャパン株式会社　</span><br><span class="ilbk">東京海上日動火災保険株式会社　</span><br><span class="ilbk">日新火災海上保険株式会社　</span><br><span class="ilbk">三井住友海上火災保険株式会社</span></dd>
						</dl>
					</article>
				</div>

			</article>
		</div>
	</section>

	<!-- <section id="sec_content03" class="bgcl_gray">
		<div class="contents_body_03">
			<article class="policy_wrap">

				<h2 class="c_ttl">
					勧誘方針
				</h2>
				<div class="c_underbar"></div>
				<p class="policy_txt">「金融商品の販売等に関する法律」に基づき、当代理店の金融商品の勧誘方針を次のとおり定めておりますので、ご案内いたします。</p>
				<div class="policy_box">
					<ul class="">
						<li class="policy_item">
							<h3 class="item_no">１.</h3>
							<p>保険法、保険業法、金融商品の販売等に関する法律、金融商品取引法、消費者契約法、個人情報の保護に関する法律およびその他各種法令等を遵守し、適正な商品販売に努めてまいります。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">２.</h3>
							<p>お客さまに商品内容を十分ご理解いただけるよう、知識の修得、研さんに励むとともに、説明方法等について工夫し、わかりやすい説明に努めてまいります。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">３.</h3>
							<p>お客さまの商品に関する知識、経験、財産の状況および購入の目的等を総合的に勘案し、お客さまに適切な商品をご選択いただけるよう、お客さまのご意向と実情に沿った説明に努めてまいります。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">４.</h3>
							<p>市場の動向に大きく影響される投資性商品については、リスクの内容について、適切な説明に努めてまいります。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">５.</h3>
							<p>商品の販売にあたっては、お客さまの立場に立って、時間帯、場所、方法等について十分配慮いたします。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">６.</h3>
							<p>お客さまに関する情報については、適正に取扱うとともに厳正に管理いたします。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">７.</h3>
							<p>お客さまのご意見、ご要望等を、商品ご提供の参考にさせていただくよう努めてまいります。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">８.</h3>
							<p>万一保険事故が発生した場合には、保険金のご請求にあたり適切な助言を行うよう努めてまいります。</p>
						</li>
						<li class="policy_item">
							<h3 class="item_no">９.</h3>
							<p>保険金を不正に取得されることを防止する観点から、適正に保険金額を定める等、適切な商品の販売に努めてまいります。</p>
						</li>
					</ul>
				</div>


			</article>
		</div>
	</section> -->

	<!--
	<section id="sec_content04" class="" >
		<div class="contents_body_03">
			<article class="ope_wrap">
				<h2 class="c_ttl">
					顧客本位の業務運営
				</h2>
				<div class="c_underbar"></div>

				<ul class="ope_box">
					<li class="ope_item">
						<h3 class="ope_ttl">見出し見出し見出し</h3>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</li>

					<li class="ope_item">
						<h3 class="ope_ttl">見出し見出し見出し</h3>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</li>

				</ul>
			
			</article>
		</div>
	</section>
-->

</main>



<?php get_footer(); ?>