<?php
/*
Template Name: 交通アクセス
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<main class="access">

	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero">
<!--
					<img src="<?php echo get_template_directory_uri(); ?>/images/access/hero_pc.jpg" class="image-switch" alt="交通アクセス">
-->
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">交通アクセス</h1>
						<p class="hed_ttlen overpass">ACCESS</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap_02">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">交通アクセス</div>
					</div>
				</div>
			</article>
		</div>

	</section>


	<section id="sec_content02">
		<div class="contents_body_03">
			<article class="access_wrap">

				<div class="conline access_conlinem"></div>

				<div class="map_box">
					<article class="map_infoarea">
						<h2 class="map_ttl ">本社</h2>
						<h3 class="map_subttl">総務課、人材情報課、営業推進課、傷害医療保険課</h3>
						<h4 class="map_infottl syuozai_m">所在地</h4>
						<p class="map_info">〒162-0846　東京都新宿区市谷左内町29-4</p>
						<a class="combtn" href="https://goo.gl/maps/xNbGiKRGq47TWRZP6" target="_blank">Google Mapで開く<img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_r_wh.svg" class="arw_icon" alt=">"></a>
						<h4 class="map_infottl annai_m">交通案内</h4>
						<ul class="sqlist">
							<li class="sqitem">JR市ケ谷駅　徒歩7分
							</li>
							<li class="sqitem">有楽町線／南北線　市ケ谷駅エレベーター出口　徒歩6分
							</li>
							<li class="sqitem">有楽町線／南北線　市ケ谷駅6番出口　徒歩7分
							</li>
							<li class="sqitem">都営新宿線　市ヶ谷駅　徒歩8分
							</li>
						</ul>
					</article>
					<aside class="maparea">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.292064727025!2d139.73147721571505!3d35.69442978019118!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188c5f11406d33%3A0xe5ef9b48ff796f09!2z5byl55Sf5YWx5riI5LyaIOacrOekvu-8iOe3j-WLmeiqsuOAgeWWtualreaOqOmAsuiqsuOAgeWCt-Wus-WMu-eZguS_nemZuuiqsu-8iQ!5e0!3m2!1sja!2sjp!4v1600918683933!5m2!1sja!2sjp" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</aside>
				</div>

				<div class="conline access_conlinem"></div>

				<div class="map_box">
					<article class="map_infoarea">
						<h2 class="map_ttl ">市谷支店</h2>
						<h3 class="map_subttl">自動車保険課</h3>
						<h4 class="map_infottl syuozai_m">所在地</h4>
						<p class="map_info">〒162-0846　新宿区市谷左内町21　山上ビルＧ階</p>
						<a class="combtn" href="https://goo.gl/maps/w4jYUS56JwwREQA38" target="_blank">Google Mapで開く<img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_r_wh.svg" class="arw_icon" alt=">"></a>
						<h4 class="map_infottl annai_m">交通案内</h4>
						<ul class="sqlist">
							<li class="sqitem">JR市ケ谷駅　徒歩5分
							</li>
							<li class="sqitem">有楽町線／南北線　市ケ谷駅エレベーター出口　徒歩2分
							</li>
							<li class="sqitem">有楽町線／南北線　市ケ谷駅6番出口　徒歩3分
							</li>
							<li class="sqitem">都営新宿線　市ヶ谷駅　徒歩6分
							</li>
						</ul>
					</article>
					<aside class="maparea">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d405.0409241691294!2d139.73552934350366!3d35.69356024316905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188c5e43f63133%3A0xc012fc5b2d7e9e78!2z44CSMTYyLTA4NDYg5p2x5Lqs6YO95paw5a6_5Yy65biC6LC35bem5YaF55S6IElDSElHQVlBIFlBTUFLQU1JIEJMRC4!5e0!3m2!1sja!2sjp!4v1602572449253!5m2!1sja!2sjp" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</aside>
				</div>

			</article>
		</div>
	</section>



</main>



<?php get_footer(); ?>