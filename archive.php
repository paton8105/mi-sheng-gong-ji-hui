<?php
/*
Template Name: お知らせ一覧
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<main class="news">
	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero">
<!--
					<img src="<?php echo get_template_directory_uri(); ?>/images/news/hero_pc.jpg" class="image-switch" alt="お知らせ一覧">
-->
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">お知らせ一覧</h1>
						<p class="hed_ttlen overpass">NEWS</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">お知らせ一覧</div>
					</div>
				</div>
			</article>
		</div>
	</section>
	<section id="sec_content01" >
		<div class="contents_body_01">
			<div class="news_wrap">
				<article class="article_list">
					<ul class="post_list">
					<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<li>
							<a class="news_item" href="<?php the_permalink(); ?>">
							<?php
								$category = get_the_category();
								$cat_id   = $category[0]->cat_ID;
								$post_id = 'category_'.$cat_id;
								$cat_name = $category[0]->cat_name;
								$cat_slug = $category[0]->category_nicename;
							?>
								<div class="post_date overpass"><?php the_time( 'Y.m.d' ); ?></div>
								<div class="post_cat"><?php echo $cat_name; ?></div>
								<div class="post_ttl"><?php the_title(); ?></div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
							</a>
						</li>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					</ul>
					<div class="page_nav_wrap">
						<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
<!--
						<div class='wp-pagenavi' role='navigation'> <span class='pages'>2 / 5</span><a class="previouspostslink" rel="prev" href=""> </a><a class="page smaller" title="Page 1" href="">1</a><span aria-current='page' class='current'>2</span><a class="page larger" title="Page 3" href="">3</a><a class="page larger" title="Page 4" href="">4</a><a class="page larger" title="Page 5" href="page/5/">5</a><a class="nextpostslink" rel="next" href=""> </a> </div>
-->
					</div>
				</article>
				<aside class="archive_list">
					<div class="archive_listbg">
						<div class="archive_wrap">
							<h2 class="archive_listttl overpass">ARCHIVES</h2>
							<ul class="archive_listbox">
								<?php wp_get_archives( 'type=yearly' ); ?>
<!--
								<li><a class="archive_item active overpass" href="">2020</a></li>
								<li><a class="archive_item overpass" href="">2019</a></li>
								<li><a class="archive_item overpass" href="">2018</a></li>
-->
							</ul>
						</div>
						<div class="category_wrap">
							<h2 class="archive_listttl">CATEGORY</h2>
<ul class="cat_list">
								<?php
									//一番親階層のカテゴリをすべて取得
									$categories = get_categories('parent=0');
									
									//取得したカテゴリへの各種処理
									foreach($categories as $val){
										//カテゴリのリンクURLを取得
										$cat_link = get_category_link($val->cat_ID);
										//親カテゴリのリスト出力
										echo '<li>';
										echo '<div class="overflow"><a class="cat_item" href="' . $cat_link . '"><span class="cat_name">' . $val -> name . '</span><span class="cal_items overpass">' . $val ->count . '</span></a>';
										
										//子カテゴリのIDを配列で取得。配列の長さを変数に格納
										$child_cat_num = count(get_term_children($val->cat_ID,'category'));
										
										//子カテゴリが存在する場合
										if($child_cat_num > 0){
											echo '<ul>';
											//子カテゴリの一覧取得条件
											$category_children_args = array('parent'=>$val->cat_ID);
											//子カテゴリの一覧取得
											$category_children = get_categories($category_children_args);
											//子カテゴリの数だけリスト出力
											foreach($category_children as $child_val){
												$cat_link = get_category_link($child_val -> cat_ID);
												echo '<li><a href="' . $cat_link . '">' . $child_val -> name . '</a>';
											}
											echo '</ul>';
										}
										echo '</div></li>';
									}
								?>
							</ul>
						</div>
					</div>
				</aside>
				
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
