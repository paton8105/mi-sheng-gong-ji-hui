<?php
/*
Template Name: トップページ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<section id="sec01">
  <div class="contents_wrap">
    <div class="swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide"><img class="slide-change" src="<?php echo get_template_directory_uri(); ?>/images/top/slide_01_pc.jpg" alt="株式会社弥生共済会"></div>
        <div class="swiper-slide"><img class="slide-change" src="<?php echo get_template_directory_uri(); ?>/images/top/slide_02_pc.jpg" alt="株式会社弥生共済会"></div>
        <div class="swiper-slide"><img class="slide-change" src="<?php echo get_template_directory_uri(); ?>/images/top/slide_03_pc.jpg" alt="株式会社弥生共済会"></div>
        <div class="swiper-slide"><img class="slide-change" src="<?php echo get_template_directory_uri(); ?>/images/top/slide_04_pc.jpg" alt="株式会社弥生共済会"></div>
      </div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div>
      <div class="hero_txt_area">
        <h1>ともに、未来を支える</h1>
        <p>警視庁職員・OB、<br class="tb_br">ならびにそのご家族のみなさまの<br>
          さまざまなニーズに応じた各種保険を<br class="tb_br">お取扱いしています。</p>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="nav_contents_wrap">
    <nav class="nav_wrap">
      <ul class="nav">
        <li><a href="<?php echo home_url(); ?>">トップページ<span class="overpass">HOME</span></a></li>
        <li><a href="<?php echo home_url(); ?>/news/">お知らせ<span class="overpass">NEWS</span></a></li>
        <li><a href="<?php echo home_url(); ?>/about/">企業情報<span class="overpass">ABOUT US</span></a></li>
        <li><a href="<?php echo home_url(); ?>/access/">交通アクセス<span class="overpass">ACCESS</span></a></li>
        <li><a href="<?php echo home_url(); ?>/faq/">よくある質問<span class="overpass">FAQ</span></a></li>
        <li><a href="<?php echo home_url(); ?>/contact/">お問合せ<span class="overpass">CONTACT</span></a></li>
      </ul>
    </nav>
  </div>
</section>
<section id="sec045" class="eg1_mtg">
  <h2 class="eg1_mtg_ttl">自動車保険のご継続のお手続き（三井住友海上）</h2>
  <p class="eg1_mtg_txt">下記バナーをクリックし、「ご契約者さま専用ページ」から<br>自働車保険（三井住友海上）のご継続のお手続きいただけます。</p>
  <a href="https://opk.ms-ins.com/servlet/jsp/47871.htm" target="_blank" rel="noreferrer">
  <span class="click_img_wrap">こちらをクリック<img src="<?php echo get_template_directory_uri(); ?>/images/top/click.svg" class="click_img" alt="こちらをクリック"></span><img src="<?php echo get_template_directory_uri(); ?>/images/top/banner_g1.jpg" alt="e-G1"></a>
</section>
<div id="login" class="top_contents_wrap">
  <div class="login_area pc_login">
    <div class="inner_01">
      <section id="sec02">
        <div class="login_box">
          <p>自警会団体損害保険のパンフレットは、<br>下記フォームにパスワードを入力し、<br>ログインしてご確認ください。</p>
          <h2 class="login_ttl">会員専用ログイン</h2>
          <div class="login_form">
            <p class="login_info">ログイン後に取扱保険情報の詳細をご覧いただけます。</p>
            <form name="mlogin" method="post" enctype="multipart/form-data" action="<?php echo esc_url( home_url('/') ); ?>">
              <label class="pass_input_head">パスワード</label>
              <input class="pass_input" type="password" name="pass" size="40" maxlength="200" value="<?php echo ($pass)?$pass:"";?>" required />
              <div class="login_submit">
                <input class="login_submit_bt" type="submit" value="ログイン" /><img src="<?php echo get_template_directory_uri(); ?>/images/top/more_btn.svg" alt="右矢印" class="arw_icon">
								<input type="hidden" name="action" value="loginchk">
              </div>
							<div class=""><?php echo $msg ?></div>
            </form>
            <?php /* <a href="<?php echo home_url(); ?>/forgot/" class="forget_txt">パスワードが不明な方はこちらをご覧ください</a> */ ?>
          </div>
        </div>
      </section>
    </div>
  </div>
  <div class="news_area">
    <div class="inner_01">
			<div class="news_area_inner_cwrap">
      <div class="news_area_inner">
        <section id="sec03">
          <h2 class="sec_ttl">お知らせ</h2>
          <p class="sec_ttl_en overpass">NEWS</p>
          <ul class="news_box_list">
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array(
              'posts_per_page' => 4,
              'paged' => $paged,
              'post_type' => array(
                'post',
              ),
            );
            $the_query = new WP_Query($args);
            ?>
            <?php if ($the_query->have_posts()) : ?>
              <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <?php
                $category = get_the_category();
                $cat_id   = $category[0]->cat_ID;
                $post_id = 'category_' . $cat_id;
                $cat_name = $category[0]->cat_name;
                $cat_slug = $category[0]->category_nicename;
                /*
              $cat_txt = get_field('cat_txt',$post_id);
              $cat_bg = get_field('cat_bg',$post_id);
  */
                ?>
                <li>
                  <a href="<?php the_permalink(); ?>" class="news_list">
                    <dt class="news_date overpass"><?php the_time('Y.m.d'); ?></dt>
                    <dt class="news_type">
                      <p><?php echo $cat_name; ?></p>
                    </dt>
                    <dd class="news_txt"><?php the_title(); ?></dd>
                  </a>
                </li>
                <?php endwhile; ?>
              <?php endif; ?>
              <?php wp_reset_postdata(); ?>
            </ul>
          <div class="more_btn_wrap">
            <a href="<?php echo home_url(); ?>/news/" class="more_btn">
              <p>お知らせ一覧を見る</p>
              <img src="<?php echo get_template_directory_uri(); ?>/images/top/more_btn.svg" alt="右矢印" class="arw_r">
            </a>
          </div>
        </section>
        <div class="login_area sp_login">
          <div class="inner_01">
            <section id="sec02">
              <div class="login_box">
                <p>自警会団体損害保険のパンフレットは、<br>下記フォームにパスワードを入力し、<br>ログインしてご確認ください。</p>
                <h2 class="login_ttl">会員専用ログイン</h2>
                <div class="login_form">
                  <p class="login_info">ログイン後に取扱保険情報の詳細をご覧いただけます。</p>
                  <form name="mlogin" method="post" enctype="multipart/form-data" action="<?php echo esc_url( home_url('/') ); ?>">
                    <label class="pass_input_head">パスワード</label>
                    <input class="pass_input" type="password" name="pass" size="40" maxlength="200" value="<?php echo ($pass)?$pass:"";?>" required />
                    <div class="login_submit">
                      <input class="login_submit_bt" type="submit" value="ログイン" /><img src="<?php echo get_template_directory_uri(); ?>/images/top/more_btn.svg" alt="右矢印" class="arw_icon">
                      <input type="hidden" name="action" value="loginchk">
                    </div>
                    <div class=""><?php echo $msg ?></div>
                  </form>
                  <?php /* <a href="<?php echo home_url(); ?>/forgot/" class="forget_txt">パスワードが不明な方はこちらをご覧ください</a> */ ?>
                </div>
              </div>
            </section>
          </div>
        </div>
        <section id="sec04">
          <div class="pick_up_area">
            <h2 class="sec_ttl">ピックアップ</h2>
            <p class="sec_ttl_en overpass">PICK UP</p>
            <ul class="pick_up_list">
              <li><a href="<?php echo home_url(); ?>/about/"><img src="<?php echo get_template_directory_uri(); ?>/images/top/pick_up_01.jpg" alt="企業情報"></a></li>
              <li><a href="<?php echo home_url(); ?>/access/"><img src="<?php echo get_template_directory_uri(); ?>/images/top/pick_up_02.jpg" alt="交通アクセス"></a></li>
              <li><a href="<?php echo home_url(); ?>/faq/"><img src="<?php echo get_template_directory_uri(); ?>/images/top/pick_up_03.jpg" alt="よくある質問"></a></li>
              <li><a href="<?php echo home_url(); ?>/contact/"><img src="<?php echo get_template_directory_uri(); ?>/images/top/pick_up_04.jpg" alt="お問合せ"></a></li>
            </ul>
            <div class="policy_list">
              <a href="<?php echo home_url(); ?>/privacy-policy/"><span>個人情報保護方針</span><img src="<?php echo get_template_directory_uri(); ?>/images/top/policy_arw.svg" alt="個人情報保護方針"></a><br>
              <a href="<?php echo home_url(); ?>/solicitation-policy/"><span>勧誘方針</span><img src="<?php echo get_template_directory_uri(); ?>/images/top/policy_arw.svg" alt="勧誘方針"></a><br>
              <a href="<?php echo home_url(); ?>/customer-first/"><span>「お客様本位の業務運営」に関する方針</span><img src="<?php echo get_template_directory_uri(); ?>/images/top/policy_arw.svg" alt="「お客様本位の業務運営」に関する方針"></a>
            </div>
          </div>
        </section>
      </div>
				</div>
    </div>
  </div>
</div>
<section id="sec05">
  <div class="ic_wrap">
    <div class="contents_wrap">
      <div class="contents_body_06">
        <div class="inner_01">
          <div class="sec_box">
            <div class="sec_boxitem_01">
              <h2 class="sec_ttl">取扱保険会社</h2>
              <p class="sec_ttl_en overpass">INSURANCE COMPANY</p>
            </div>

            <div class="sec_boxitem_02">
              <ul class="ic_list">
                <li class="ic_listitem"><a href="https://www.aioinissaydowa.co.jp/" target="_blank" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/top/banner03.jpg" alt="MS&AD"></a></li>
                <li class="ic_listitem"><a href="https://www.sompo-japan.co.jp/" target="_blank" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/top/banner05.jpg" alt="損保ジャパン"></a></li>
                <li class="ic_listitem"><a href="https://www.tokiomarine-nichido.co.jp/" target="_blank" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/top/banner04.jpg" alt="東京海上日動"></a></li>
                <li class="ic_listitem"><a href="https://www.nisshinfire.co.jp/" target="_blank" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/top/banner02.jpg" alt="日新火災"></a></li>
                <li class="ic_listitem"><a href="https://www.ms-ins.com/" target="_blank" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/images/top/banner01.jpg" alt="三井住友海上"></a></li>
              </ul>
              <div class="company_list">
                <p class="company_list_item">あいおいニッセイ同和損害保険株式会社　承認番号 B21-103504（2021年12月承認）</p>
                <p class="company_list_item">損害保険ジャパン株式会社　承認番号 SJ22-11430(2022/12/05)</p>
                <p class="company_list_item">東京海上日動火災保険株式会社　承認番号 21-TC07985</p>
                <p class="company_list_item">日新火災海上保険株式会社　承認番号 NH2012-0001</p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>