<?php
$post = $wp_query->post;
if ( is_singular('blog') ) {
include(TEMPLATEPATH.'/single-blog.php');
} elseif ( is_singular('staff_list') ) {
include(TEMPLATEPATH.'/single-staff.php');
} else {
include(TEMPLATEPATH.'/single-default.php');
}
?>
