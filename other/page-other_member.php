<?php
/*
Template Name: 火災保険会員ページ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
	<section id="sec01">
		<div class="menber_band">自警会会員専用ページ</div>
		<div class="hero submember_bg">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">火災保険 その他</span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
				</div>
			</div>
		</div>
		<div class="breadcrumbs_wrap">
			<div class="contents_body_03">
				<div class="breadcrumbs submember_breadcrumbs">
					<div class="breadcrumbs_list"><a href="../../../">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../">自動車保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list">火災保険 その他(会員ページ)</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec02" class="submember">
		<div class="contents_body_03">
			<h2 class="submember_ttl">火災保険 その他</h2>
			<div class="submemer_contents_box">
				<div class="submember_pdf">
					<figure><img src="<?php the_field('thum'); ?>" alt=""></figure>
				</div>
				<div class="text_wrap">
					<div class="text_box">
						<p class="text_box_ttl">火災保険の特徴</p>
						<p class="text_box_txt">
							テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
						</p>
					</div>
					<div class="download_wrap mt_s">
						<a href="<?php the_field('file'); ?>" target="_blank" class="download_link">
							<p class="download_link_pdf"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_pdf.svg" alt="pdf">パンフレット</p>
							<p class="download_link_download overpass">DOWNLOAD<img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_download.svg" alt="" class="download_link_download_icon_on"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_download_on.svg" alt=""></p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec03">

	</section>
	<section id="sec04">



	</section>
<?php get_footer(); ?>
