<?php mlogin_chk() ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MLSX7VP');</script>
<!-- End Google Tag Manager -->
<meta charset="UTF-8">
<title><?php global $page, $paged; if(is_front_page()) : bloginfo('name'); elseif(is_home()) : wp_title('|', true, 'right'); bloginfo('name'); elseif(is_single()) : wp_title(''); elseif(is_page()) : wp_title('|', true, 'right'); bloginfo('name'); elseif(is_author()): wp_title('|',  true, 'right'); bloginfo('name'); elseif(is_archive()) : wp_title('|', true, 'right'); bloginfo('name'); elseif(is_search()) : wp_title(''); elseif(is_404()): echo '404|'; bloginfo('name'); endif;	if($paged >= 2 || $page >= 2) :	echo '-' . sprintf('%sページ', max($paged,$page)); endif; ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
