<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/loader.css">
<?php wp_head() ?>
</head>

<body class="preload">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MLSX7VP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="nano">
	<div class="nano-content">
<!--
	<div class="loading">
		<div class="loader" id="loader_wrap">
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
-->
<div id="my-spinner" class="box">
	<div class="loadpos">
		<div class="loader"></div>
		<div class="spinner2">
		<!-- 	<span>Loading...</span> -->
		</div>
	</div>
</div>
	<div class="page_top"><a href=""></a></div>
<a class="nav_btn">
	<span class="nav_btn_line_wrap">
		<span class="nav_btn_line nav_btn_line-top"></span>
		<span class="nav_btn_line nav_btn_line-center"></span>
		<span class="nav_btn_line nav_btn_line-bottom"></span>
	</span>
	<span class="nav_btn_txt">メニュー</span>
</a>
	<header>
		<div class="header_contents">
			<div class="logo">
				<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/common/logo_pc.png" alt="株式会社弥生共済会" /></a>
			</div>
			<a href="<?php echo home_url(); ?>/access/" class="head_access_btn">
				<div class="head_access_btn_inner">
					<img src="<?php echo get_template_directory_uri(); ?>/images/common/icon_access.svg" alt="アクセス">
					<span>アクセス</span>
				</div>
			</a>
<!--
			<div class="head_tel">
				<div class="tel_number">
					<span class="overpass">TEL.</span>
					<span class="overpass">03-3269-0849</span>
				</div>
				<div class="business_hours">
					<p>受付時間　9:00～16:00&nbsp;年末年始・土日・祝日を除く</p>
				</div>
			</div>
-->
		<?php /* <div class="member_login">
			<span class="login_txt">取扱保険の詳細はこちら</span>
			<a href="<?php echo home_url(); ?>/member/" class="login_btn">会員専用ログイン<img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_r_wh.svg" class="arw_icon" alt=">"></a>
		</div> */ ?>
		</div>
		<div class="sc_nav top_floting">
			<div class="contents_wrap">
				<ul class="top_nav_btn_box">
					<li><a class="top_nav_btn" href="<?php echo home_url(); ?>">トップページ</a></li>
					<li><a class="top_nav_btn" href="<?php echo home_url(); ?>/news/">お知らせ</a></li>
					<li><a class="top_nav_btn" href="<?php echo home_url(); ?>/about/">企業情報</a></li>
					<li><a class="top_nav_btn" href="<?php echo home_url(); ?>/access/">交通アクセス</a></li>
					<li><a class="top_nav_btn" href="<?php echo home_url(); ?>/faq/">よくある質問</a></li>
					<li><a class="top_nav_btn" href="<?php echo home_url(); ?>/contact/">お問合せ</a></li>
				</ul>
			</div>
		</div>
		<nav class="gnav">
			<div class="gnav_inner">
				<div class="nav_list_wrap">
					<div class="gnav_login_btn_wrap">
						<span class="gnav_login_txt">取扱保険の詳細はこちら</span>
						<a href="<?php echo home_url(); ?>/member/" class="gnav_login_btn">会員専用ログイン<img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_r_wh.svg" class="arw_icon" alt=">"></a>
					</div>
					<div class="nav_list">
						<ul>
							<li>
								<a href="<?php echo home_url(); ?>">
									<span class="nav_list_ja">トップページ</span>
									<span class="nav_list_en overpass">HOME</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/news/">
									<span class="nav_list_ja">お知らせ</span>
									<span class="nav_list_en overpass">NEWS</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/about/">
									<span class="nav_list_ja">企業情報</span>
									<span class="nav_list_en overpass">ABOUT US</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/access/">
									<span class="nav_list_ja">交通アクセス</span>
									<span class="nav_list_en overpass">ACCESS</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/faq/">
									<span class="nav_list_ja">よくある質問</span>
									<span class="nav_list_en overpass">FAQ</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/contact/">
									<span class="nav_list_ja">お問合せ</span>
									<span class="nav_list_en overpass">CONTACT</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/privacy-policy/">
									<span class="nav_list_ja">個人情報保護方針</span>
									<span class="nav_list_en overpass">PRIVACY POLICY</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/solicitation-policy/">
									<span class="nav_list_ja">勧誘方針</span>
									<span class="nav_list_en overpass">SOLICITATION POLICY</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
							<li>
								<a href="<?php echo home_url(); ?>/customer-first/">
									<span class="nav_list_ja">「お客様本位の業務運営」に関する方針</span>
									<span class="nav_list_en overpass">CUSTOMER-FIRST BUSINESS PRACTICES</span>
									<span class="arw_nav_list"></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<div class="wrap">