<?php
/*
Template Name: テストお知らせ一覧
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<main class="news">
	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero"> <img src="<?php echo get_template_directory_uri(); ?>/images/news/hero_pc.jpg" class="image-switch" alt="お知らせ一覧">
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">お知らせ一覧</h1>
						<p class="hed_ttlen overpass">NEWS</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap_02">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">お知らせ一覧</div>
					</div>
				</div>
			</article>
		</div>
	</section>
	<section id="sec_content01">
		<div class="contents_body_01">
			<div class="news_wrap">
				<article class="article_list">
					<ul class="post_list">
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
						<li>
							<div class="post_date overpass">2020.00.00</div>
							<div class="post_cat">お知らせ</div>
							<div class="post_ttl">ホームページをオープンしました</div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/news_arw_r.svg" class="news_icon">
						</li>
					</ul>
					<div class="page_nav_wrap">
						<div class='wp-pagenavi' role='navigation'> <span class='pages'>2 / 5</span><a class="previouspostslink" rel="prev" href=""> </a><a class="page smaller" title="Page 1" href="">1</a><span aria-current='page' class='current'>2</span><a class="page larger" title="Page 3" href="">3</a><a class="page larger" title="Page 4" href="">4</a><a class="page larger" title="Page 5" href="page/5/">5</a><a class="nextpostslink" rel="next" href=""> </a> </div>
					</div>
				</article>
				<aside class="archive_list">
					<div class="archive_listbg">
						<div class="archive_wrap">
							<h2 class="archive_listttl overpass">ARCHIVES</h2>
							<ul class="archive_listbox">
								<li><a class="archive_item active overpass" href="">2020</a></li>
								<li><a class="archive_item overpass" href="">2019</a></li>
								<li><a class="archive_item overpass" href="">2018</a></li>
							</ul>
						</div>
						<div class="category_wrap">
							<h2 class="archive_listttl">CATEGORY</h2>
							<ul class="cat_list">
								<li><a class="cat_item" href=""><span class="cat_name">お知らせ</span><span class="cal_items overpass">18</span></a></li>
								<li><a class="cat_item" href=""><span class="cat_name">キャンペーン</span><span class="cal_items overpass">4</span></a></li>
							</ul>
						</div>
					</div>
				</aside>

			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>