<?php
/*
Template Name: 火災保険 その他
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<main class="group_insurance">

	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero">
<!--
					<img src="<?php echo get_template_directory_uri(); ?>/images/other/hero_pc.jpg" class="image-switch" alt="火災保険 その他">
-->
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">火災保険 その他</h1>
						<p class="hed_ttlen overpass">INSURANCE</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">火災保険 その他</div>
					</div>
				</div>
			</article>
		</div>

	</section>

	<section id="sec_content01">
		<div class="contents_body_03">
			<article class="conhed_wrap">
				<figure class="conhed_bg"></figure>
				<div class="conhed_box">
					<h2 class="conhed_ttl">〇〇〇〇課</h2>
					<p class="conhed_tel overpass">TEL 0120-000-000</p>
					<p class="conhed_time">受付時間　9:00～16:00</p>
					<p class="conhed_txt">取引保険会社の商品の中から、お客様のニーズに一番あった商品をご提案させていただきます。詳細は自警会会員専用ページへログインしてください。</p>
					<a class="combtn" href="<?php echo home_url(); ?>/other/member/" >会員専用ページで詳しく見る<img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_r_wh.svg" class="arw_icon"></a>
				</div>

			</article>
		</div>
	</section>




</main>



<?php get_footer(); ?>