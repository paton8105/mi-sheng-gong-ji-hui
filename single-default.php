<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<main class="news single">
	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<div class="news_single_wrap"></div>
				<div class="breadcrumbs_wrap">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>/news/">お知らせ一覧</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list"><?php the_title(); ?></div>
					</div>
				</div>
<!--
				<div class="breadcrumbs_line"></div>
-->
			</article>
		</div>
	</section>
	<section id="sec_content01">
		<div class="contents_body_01">
			<div class="news_wrap">
				<article class="article_list">
				<?php if (have_posts()): ?>
					<?php while (have_posts()) : the_post(); ?>
					<?php
						$category = get_the_category();
						$cat_id   = $category[0]->cat_ID;
						$post_id = 'category_'.$cat_id;
						$cat_name = $category[0]->cat_name;
						$cat_slug = $category[0]->category_nicename;
					?>
					<div class="single_ttlbox">
						<div class="sigle_post_date overpass"><?php the_time( 'Y.m.d' ); ?></div>
						<div class="sigle_post_cat"><?php echo $cat_name; ?></div>
						<div class="sigle_post_ttl"><?php the_title(); ?></div>
					</div>

					<div class="single_content">
						<?php the_content(); ?>
					</div>
					<div class="single_link">
						<div class="post_prev">
							<?php previous_post_link('%link', 'Prev'); ?>
						</div>
						<div class="back_btn"><a href="<?php echo home_url(); ?>/news/">一覧へ</a></div>
						<div class="post_next">
							<?php next_post_link('%link', 'Next'); ?>
						</div>
					</div>
					<?php endwhile; ?>
				<?php else: ?>
					<p>記事がありません。</p>
				<?php endif; ?>

				</article>

				<aside class="archive_list">
					<div class="archive_listbg">
						<div class="archive_wrap">
							<h2 class="archive_listttl overpass">ARCHIVES</h2>
							<ul class="archive_listbox">
								<?php wp_get_archives( 'type=yearly' ); ?>
<!--
								<li><a class="archive_item active overpass" href="">2020</a></li>
								<li><a class="archive_item overpass" href="">2019</a></li>
								<li><a class="archive_item overpass" href="">2018</a></li>
-->
							</ul>
						</div>
						<div class="category_wrap">
							<h2 class="archive_listttl">CATEGORY</h2>
							<ul class="cat_list">
							<?php $categories = get_categories(); ?>
							<?php foreach($categories as $category) : ?>
								<li><a class="cat_item" href="<?php echo home_url(); ?>/news/<?php echo $category->category_nicename; ?>"><span class="cat_name"><?php echo $category->name; ?></span><span class="cal_items overpass"><?php echo $category->count; ?></span></a></li>
							<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</aside>

			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>