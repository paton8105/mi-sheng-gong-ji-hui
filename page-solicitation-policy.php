<?php
/*
Template Name: 勧誘方針
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<main class="solicitation_policy">
  <section id="sec01">
    <div class="hero">
      <div class="pp_page_ttl_wrap">
        <div class="page_body_01">
          <div class="page_ttl">
            <h1><span class="page_ttl_ja">勧誘方針</span><span class="pp_page_ttl_en overpass">SOLICITATION POLICY</span></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="breadcrumbs_wrap">
      <div class="contents_body_03">
        <div class="breadcrumbs submember_breadcrumbs">
          <div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
          <div class="breadcrumbs_list arw_breadcrumbs">〉</div>
          <div class="breadcrumbs_list">勧誘方針</div>
        </div>
      </div>
    </div>
  </section>
  <section id="sec_content03" class="bgcl_gray">
    <div class="contents_body_03">
      <article class="policy_wrap">
        <p class="policy_txt">「金融サービスの提供及び利用環境の整備等に関する法律」に基づき、当代理店の金融商品の勧誘方針を次のとおり定めておりますので、ご案内いたします。</p>
        <div class="policy_box">
          <ul class="policy_list">
            <li class="policy_item">
              <h3 class="item_no">１.</h3>
              <p>保険法、保険業法、金融サービスの提供に関する法律、金融商品取引法、消費者契約法、個人情報の保護に関する法律およびその他各種法令等を遵守し、適正な商品販売に努めてまいります。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">２.</h3>
              <p>お客さまに商品内容を十分ご理解いただけるよう、知識の修得、研さんに励むとともに、説明方法等について工夫し、わかりやすい説明に努めてまいります。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">３.</h3>
              <p>お客さまの商品に関する知識、経験、財産の状況および購入の目的等を総合的に勘案し、お客さまに適切な商品をご選択いただけるよう、お客さまのご意向と実情に沿った説明に努めてまいります。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">４.</h3>
              <p>市場の動向に大きく影響される投資性商品については、リスクの内容について、適切な説明に努めてまいります。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">５.</h3>
              <p>商品の販売にあたっては、お客さまの立場に立って、時間帯、場所、方法等について十分配慮いたします。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">６.</h3>
              <p>お客さまに関する情報については、適正に取扱うとともに厳正に管理いたします。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">７.</h3>
              <p>お客さまのご意見、ご要望等を、商品ご提供の参考にさせていただくよう努めてまいります。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">８.</h3>
              <p>万一保険事故が発生した場合には、保険金のご請求にあたり適切な助言を行うよう努めてまいります。</p>
            </li>
            <li class="policy_item">
              <h3 class="item_no">９.</h3>
              <p>保険金を不正に取得されることを防止する観点から、適正に保険金額を定める等、適切な商品の販売に努めてまいります。</p>
            </li>
          </ul>
        </div>
      </article>
    </div>
  </section>

</main>
<?php get_footer(); ?>