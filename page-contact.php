<?php
/*
Template Name: お問合せ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
	<section id="sec01">
		<div class="contents_body_01">
			<article class="hed_wrap">
				<figure class="hedhero">
<!--
					<img src="<?php echo get_template_directory_uri(); ?>/images/faq/hero_pc.jpg" class="image-switch" alt="よくある質問">
-->
					<figcaption class="hed_ttlbox">
						<h1 class="hed_ttl">お問合せ</h1>
						<p class="hed_ttlen overpass">CONTACT</p>
					</figcaption>
				</figure>
				<div class="breadcrumbs_wrap_02">
					<div class="breadcrumbs">
						<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
						<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
						<div class="breadcrumbs_list">お問合せ</div>
					</div>
				</div>
			</article>
		</div>
	</section>

	
<section id="sec02">
  <div class="contents_body_03">
    <div class="inner_01">
      <div class="contact_wrap">
				<h2 class="cttl">弥生共済会へのお問合せ</h2>
				<article class="contact_conwrap">
					<p class="c_info">団体傷害保険・長期サポートプラン・医療保険・ガン保険・介護サポート保険等のお問合せは、傷害医療保険課へお願いします。</p>
					<h3 class="c_subttl">傷害医療保険課</h3>
					<p class="c_add">〒162-0846　新宿区市谷左内町29-4</p>
					<ul class="c_menu">
						<li class="c_item moadlbt cblue" data-modalconname="modal_con01">
							<span class="c_btinfo">ご契約のお手続き・事故のご連絡先</span>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/plus.svg" alt="＋" class="icon_plus">
						</li>
						<li class="c_item moadlbt cgreen" data-modalconname="modal_con02">
							<span class="c_btinfo">営業時間外の事故のご連絡先</span>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/plus.svg" alt="＋" class="icon_plus">
						</li>
					</ul>
				</article>
				<article class="contact_conwrap">
					<h3 class="c_subttl">自動車保険課</h3>
					<p class="c_add">〒162-0846　新宿区市谷左内町21　山上ビルＧ階</p>
					<ul class="c_menu">
						<li class="c_item moadlbt cgreen" data-modalconname="modal_con04">
							<span class="c_btinfo">事故のご連絡先</span>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/plus.svg" alt="＋" class="icon_plus">
						</li>
						<li class="c_item moadlbt cgreen" data-modalconname="modal_con03">
							<span class="c_btinfo">契約内容変更のご連絡先</span>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/plus.svg" alt="＋" class="icon_plus">
						</li>
						<li class="c_item moadlbt cblue" data-modalconname="modal_con05">
							<span class="c_btinfo">新規・更新・解約のご連絡先</span>
							<img src="<?php echo get_template_directory_uri(); ?>/images/common/plus.svg" alt="＋" class="icon_plus">
						</li>
					</ul>
				</article>
			</div>
      <div class="contact_wrap">
				<h2 class="cttl">その他のお問合せ</h2>
				<article class="contact_conwrap">
					<p class="c_info">生命・傷病共済、新火災共済、新長期生命80、終身生命共済、財形年金共済、ゆとり年金のお問合せは、警察職員生活協同組合へお願いします。</p>
					<h3 class="c_subttl">警察職員生活協同組合警視庁支部</h3>
					<div class="c_windowinfobox">
						<a class="c_link_green" href="https://www.keiseikyo.or.jp/solution/minaoshi/" target="_blank" rel="nofollow">警生協ホームページ<img src="<?php echo get_template_directory_uri(); ?>/images/common/exit.svg" alt="＋" class="icon_ext"></a>
					</div>
				</article>
				<article class="contact_conwrap">
					<p class="c_info">希望グループ保険、ファミリー医療保険、三大疾病医療保険、遺家族支援補償などのお問合せは<br />一般財団法人 自警会へお願いします。</p>
					<h3 class="c_subttl">一般財団法人 自警会</h3>
					<div class="c_windowinfobox">
						<p class="c_windowinfo_name">自警会（保険部）ご連絡先</p>
						<p class="c_windowinfo_tel overpass">TEL <a class="taptel" href="tel:03-3581-4321">03-3581-4321</a></p>
					</div>
					<p>（内線）7550-2950</p>
				</article>
			</div>
		</div>
	</div>
</section>
<!-- モーダルダイアログのコンテンツ -->
<div class="dialog_nodisp">

	<div class="modal_con01">
		<div class="modalcon_wrap">
			<h2 class="tb_ttl">ご契約のお手続き・事故のご連絡先</h2>
			<div class="tb_wrap tb_blue">
				<table class="chg_tb">
					<tr>
						<th class="ctb_th">弥生共済会</th>
						<th class="ctb_th">ご連絡先</th>
						<th class="ctb_th">受付時間</th>
					</tr>
					<tr>
						<td class="ctb_td "><span class="tb_bold">傷害医療保険課</span></td>
						<td class="ctb_td"><a href="tel:0120-189-841" class="taptel">0120-189-841</a></td>
						<td class="ctb_td">平日　9:00～16:00</td>
					</tr>
				</table>
				<div class="tb_sp"></div>
			</div>
		</div>
	</div>

	<div class="modal_con02">
		<div class="modalcon_wrap">
			<h2 class="tb_ttl tb_ttl_green">営業時間外の事故のご連絡先</h2>
			<div class="tb_wrap tb_green">
				<table class="chg_tb tb_green_radius">
					<tr>
						<th class="ctb_th">保険種類(保険会社名)</th>
						<th class="ctb_th" colspan="2">ご連絡先</th>
						<th class="ctb_th">受付時間</th>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">団体傷害保険<br>（日新火災）</span></td>
						<td class="ctb_td" rowspan="2"><span class="tb_bold">事故受付センター</span></td>
						<td class="ctb_td" rowspan="2"><a href="tel:0120-232-233" class="taptel">0120-232-233</a></td>
						<td class="ctb_td" rowspan="7">24時間　365日</td>
					</tr>
					<tr>
						<td class="ctb_td ctb_td_none"><span class="tb_bold">退職者用延長保険<br>（日新火災）</span></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">長期給与サポートプラン<br>（東京海上日動）</span></td>
						<td class="ctb_td"><span class="tb_bold">東京海上日動安心110番</span></td>
						<td class="ctb_td"><a href="tel:0120-720-110" class="taptel">0120-720-110</a></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">医療保険<br>（損保ジャパン）</span></td>
						<td class="ctb_td" rowspan="3"><span class="tb_bold"> 事故サポート<br>センター</span></td>
						<td class="ctb_td" rowspan="3"><a href="tel:0120-727-110" class="taptel">0120-727-110</a></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">がん保険特約<br>（損保ジャパン）</span></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">退職者総合補償制度<br>（損保ジャパン）</span></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">介護サポート保険<br>（三井住友）</span></td>
						<td class="ctb_td"><span class="tb_bold">三井住友海上事故受付センター</span></td>
						<td class="ctb_td"><a href="tel:0120-258-189" class="taptel">0120-258-189</a></td>
					</tr>
				</table>
				<div class="tb_sp"></div>
			</div>
		</div>
	</div>

	<div class="modal_con03">
		<div class="modalcon_wrap">
			<h2 class="tb_ttl tb_ttl_green">契約内容変更のご連絡先</h2>
			<div class="tb_wrap tb_green">
				<table class="chg_tb second_tb">
					<tr>
						<th class="ctb_th">保険会社名</th>
						<th class="ctb_th" colspan="2">ご連絡先</th>
						<th class="ctb_th">受付時間</th>
					</tr>
					<tr>
						<td class="ctb_td" rowspan="2"><span class="tb_bold">三井住友</span></td>
						<td class="ctb_td"><span class="tb_bold">お客様デスク</span></td>
						<td class="ctb_td"><a href="tel:0120-632-277" class="taptel">0120-632-277</a></td>
						<td class="ctb_td" rowspan="2">平日　9:00~18:00<br>土日祝　9:00~17:00</td>
					</tr>
					<tr class="no_border_right">
						<td class="ctb_td"><span class="tb_bold">変更専用</span></td>
						<td class="ctb_td"><a href="tel:0120-988-777" class="taptel">0120-988-777</a></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">日新火災</span></td>
						<td class="ctb_td"><span class="tb_bold">ご契約内容変更<br>デスク</span></td>
						<td class="ctb_td"><a href="tel:0120-612-400" class="taptel">0120-612-400</a></td>
						<td class="ctb_td">平日　9:00~18:00<br>土日祝　9:00~17:00</td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">東京海上日動</span></td>
						<td class="ctb_td"><span class="tb_bold">東京海上日動<br>カスタマーセンター</span></td>
						<td class="ctb_td"><a href="tel:0120-691-300" class="taptel">0120-691-300</a></td>
						<td class="ctb_td">受付時間は下記URLよりご確認をお願いします。<br><a href="https://www.tokiomarine-nichido.co.jp/support/contact/auto213.html" target="_blank" rel="nofollow" class="tokio_url">https://www.tokiomarine-nichido.co.jp/support/contact/auto213.html</a></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">損保ジャパン</span></td>
						<td class="ctb_td"><span class="tb_bold">カスタマーセンター</span></td>
						<td class="ctb_td"><a href="tel:0120-238-381" class="taptel">0120-238-381</a></td>
						<td class="ctb_td">平日　9:00~20:00<br>土日祝　9:00~17:00</td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">あいおいニッセイ同和損保</span></td>
						<td class="ctb_td"><span class="tb_bold">カスタマーセンター</span></td>
						<td class="ctb_td"><a href="tel:0120-101-101" class="taptel">0120-101-101</a></td>
						<td class="ctb_td">平日　9:00~18:00<br>土日祝　9:00~17:00（年末年始を除きます）</td>
				</table>
				<div class="tb_sp"></div>
			</div>
		</div>
	</div>

	<div class="modal_con04">
		<div class="modalcon_wrap">
			<h2 class="tb_ttl tb_ttl_green">事故のご連絡先</h2>
			<div class="tb_wrap tb_green">
				<table class="chg_tb  tb_green_radius">
					<tr>
						<th class="ctb_th">保険会社名</th>
						<th class="ctb_th" colspan="2">ご連絡先</th>
						<th class="ctb_th">受付時間</th>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">三井住友</span></td>
						<td class="ctb_td"><span class="tb_bold">事故受付センター</span></td>
						<td class="ctb_td"><a href="tel:0120-258-365" class="taptel">0120-258-365</a></td>
						<td class="ctb_td" rowspan="6">24時間　365日</td>
					</tr>
					<tr class="no_border_right">
						<td class="ctb_td"><span class="tb_bold">日新火災</span></td>
						<td class="ctb_td"><span class="tb_bold">事故受付センター</span></td>
						<td class="ctb_td"><a href="tel:0120-25-7474" class="taptel">0120-25-7474</a></td>
					</tr>
					<tr class="no_border_right">
						<td class="ctb_td"><span class="tb_bold">東京海上日動</span></td>
						<td class="ctb_td"><span class="tb_bold">東海日動安心110番</span></td>
						<td class="ctb_td"><a href="tel:0120-119-110" class="taptel">0120-119-110</a></td>
					</tr>
					<tr class="no_border_right">
						<td class="ctb_td" rowspan="2"><span class="tb_bold">損保ジャパン</span></td>
						<td class="ctb_td"><span class="tb_bold">事故サポート<br>センター</span></td>
						<td class="ctb_td"><a href="tel:0120-256-110" class="taptel">0120-256-110</a></td>
					</tr>
					<tr class="no_border_right">
						<td class="ctb_td"><span class="tb_bold">ロードアシスタンス専用デスク</span></td>
						<td class="ctb_td"><a href="tel:0120-365-110" class="taptel">0120-365-110</a></td>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">あいおいニッセイ同和損保</span></td>
						<td class="ctb_td"><span class="tb_bold">あいおいニッセイ<br>同和損保<br>あんしんサポート<wbr><span class="nowrap">センター</span></span></td>
						<td class="ctb_td"><a href="tel:0120-024-024" class="taptel">0120-024-024</a></td>
					</tr>
				</table>
				<div class="tb_sp"></div>
			</div>
		</div>
	</div>

	<div class="modal_con05">
		<div class="modalcon_wrap">
			<h2 class="tb_ttl">新規・更新・解約のご連絡先</h2>
			<div class="tb_wrap tb_blue">
				<table class="chg_tb ">
					<tr>
						<th class="ctb_th">弥生共済会</th>
						<th class="ctb_th">ご連絡先</th>
						<th class="ctb_th">受付時間</th>
					</tr>
					<tr>
						<td class="ctb_td"><span class="tb_bold">自動車保険課</span></td>
						<td class="ctb_td"><a href="tel:0120-279-841" class="taptel">0120-279-841</a></td>
						<td class="ctb_td">平日　9:00～16:00</td>
					</tr>
				</table>
				<div class="tb_sp"></div>
			</div>
		</div>
	</div>

</div>

<!-- モーダル -->
<div class="overlay">
<!--
	<div class="nano2">
		<div class="nano2_con">
-->
			<div class="container">
				<div class="inner">
					<section class="modal">
						<p>ここにデータが入る</p>
					</section>
					<div class="modal_closeicon"><img src="<?php echo get_template_directory_uri(); ?>/images/common/modal_dl_close_48_48.svg"alt="x"></div>
				</div>
			</div>
<!--
		</div>
	</div>
-->
</div>

<?php get_footer(); ?>