<?php
/*
Template Name: 自警会団体傷害保険会員ページ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
	<section id="sec01">
		<div class="menber_band">自警会会員専用ページ</div>
		<div class="hero submember_bg">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">自警会団体傷害保険</span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
				</div>
			</div>
		</div>
		<div class="breadcrumbs_wrap">
			<div class="contents_body_03">
				<div class="breadcrumbs submember_breadcrumbs">
					<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../../">取扱保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../">団体傷害・医療保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list">自警会団体傷害保険</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec02" class="submember">
		<div class="contents_body_03">
			<h2 class="submember_ttl">自警会団体傷害保険</h2>
			<div class="submemer_contents_box">
				<div class="submember_pdf">
					<figure><img src="<?php the_field('thum'); ?>" alt="パンフレット"></figure>
				</div>
				<div class="text_wrap">
					<div class="text_box">
						<p class="text_box_ttl">自警会団体傷害保険の特徴</p>
						<ul>
							<li>
								<p><span class="text_box_underline">団体割引等 約68%適用</span>です。<br>（「長期給与サポートプラン特約」は30%割引）</p>
							</li>
							<li>
								<p>『<span class="text_box_underline">充実した５つの特約</span>』がお勧めです。</p>
							</li>
							<li>
								<p>R6年度より、加入対象者が広がります。<br>個人型の加入対象に会員と別居の配偶者の両親と子供の配偶者を追加。</p>
							</li>	
						</ul>
					</div>
					<div class="list_wrap">
						<ul class="numbering">
							<li class="numbering_list">
								<p class="list_ttl">賠償ワイド特約</p>
								<p>日常生活の賠償事故や、保管物の賠償事故を補償します。</p>
							</li>
							<li class="numbering_list">
								<p class="list_ttl">セット特約</p>
								<p>携行品（カメラ・バッグ・レジャー用品等）の損害等を補償します。</p>
							</li>
							<li class="numbering_list">
								<p class="list_ttl">借家人賠償責任保険特約</p>
								<p>借家にお住まいの方のための安心補償です。</p>
							</li>
							<li class="numbering_list">
								<p class="list_ttl">ホールインワン・アルバトロス特約</p>
								<p>ゴルフをされる方の夢の実現に備える補償です。</p>
							</li>
							<li class="numbering_list">
								<p class="list_ttl">長期給与サポートプラン特約</p>
								<p>病気やケガで働けなくなったときに減少する給与所得を補償します。公務中・公務外を問わず、２４時間補償です。天災や精神疾患も補償します。</p>
							</li>
						</ul>
					</div>
					<div class="download_wrap">
					<?php 
						$fields = CFS()->get('pamphlet'); 
						if (!empty($fields)) {
					?>
					<?php 
              foreach( $fields as $field ) {
                if (!empty($field['pamphlet_file'])) {
                  echo '<a href="'.$field['pamphlet_file'].'" target="_blank" class="download_link">
									<p class="download_link_pdf"><img src="'.get_template_directory_uri().'/images/common/ico_pdf.svg" alt="pdf">'.$field['pamphlet_ttl'].'</p>
									<p class="download_link_download overpass">DOWNLOAD<img src="'.get_template_directory_uri().'/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="'.get_template_directory_uri().'/images/common/ico_download_on.svg" alt="↓"></p>
								</a>';
                }
              }
            ?>
						  <?php 
								}
							?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec03">

	</section>
	<section id="sec04">



	</section>
<?php get_footer(); ?>
