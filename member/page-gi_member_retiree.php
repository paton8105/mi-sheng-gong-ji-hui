<?php
/*
Template Name: 自警会退職者総合補償制度
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<section id="sec01">
	<div class="menber_band">自警会会員専用ページ</div>
	<div class="hero submember_bg">
		<div class="page_body_01">
			<div class="page_ttl">
				<h1><span class="page_ttl_ja">自警会退職者総合補償制度</span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
			</div>
		</div>
	</div>
	<div class="breadcrumbs_wrap">
		<div class="contents_body_03">
			<div class="breadcrumbs submember_breadcrumbs">
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../../">取扱保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../">団体傷害・医療保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list">自警会退職者総合補償制度</div>
			</div>
		</div>
	</div>
</section>
<section id="sec02" class="submember">
	<div class="contents_body_03">
		<h2 class="submember_ttl">自警会退職者総合補償制度</h2>
		<div class="submemer_contents_box">
			<div class="submember_pdf">
				<figure><img src="<?php the_field('thum'); ?>" alt="パンフレット"></figure>
			</div>
			<div class="text_wrap">
				<div class="text_box">
					<p class="text_box_ttl">自警会退職者総合補償制度の特長</p>
					<ul>
						<li>
							<p>3つの補償をご準備しております。</p>
						</li>
						<li>
							<p>R6年度から、抗がん剤治療補償プランが導入されました。<br>傷害保険の保険料が引き下げになりました。</p>
						</li>
					</ul>
				</div>
				<div class="list_wrap">
					<ul class="numbering">
						<li class="numbering_list">
							<p class="list_ttl">病気に備える補償・・・医療保険(医療保険基本特約・疾病保険特約・傷害保険特約セット団体総合保険)</p>
							<p>医療保険にはオプションで以下の3つの特約に加入することができます。</p>
							<ul class="list_mark_circle list_mark_circle_items">
								<li>
									<h3 class="retiree_ttl">先進医療の補償</h3>
									<p class="retiree_subtxt">先進医療等費用補償特約</p>
								</li>
								<li>
									<h3 class="retiree_ttl">介護の補償</h3>
									<p class="retiree_subtxt">介護一時金支払特約</p>
								</li>
								<li>
									<h3 class="retiree_ttl">がんに備える補償</h3>
									<p class="retiree_subtxt">がん保険特約</p>
								</li>
							</ul>
						</li>
						<li class="numbering_list">
							<p class="list_ttl">ケガに備える補償・・・傷害保険(傷害総合保険)</p>
						</li>
						<li class="numbering_list">
							<p class="list_ttl">ゴルフ中の補償・・・ゴルファー保険(賠償責任保険)</p>
						</li>
					</ul>
				</div>
				<div class="download_wrap">
					<?php 
						$fields = CFS()->get('pamphlet'); 
						if (!empty($fields)) {
					?>
					<?php 
              foreach( $fields as $field ) {
                if (!empty($field['pamphlet_file'])) {
                  echo '<a href="'.$field['pamphlet_file'].'" target="_blank" class="download_link">
									<p class="download_link_pdf"><img src="'.get_template_directory_uri().'/images/common/ico_pdf.svg" alt="pdf">'.$field['pamphlet_ttl'].'</p>
									<p class="download_link_download overpass">DOWNLOAD<img src="'.get_template_directory_uri().'/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="'.get_template_directory_uri().'/images/common/ico_download_on.svg" alt="↓"></p>
								</a>';
                }
              }
            ?>
          <?php 
            }
          ?>
				</div>
				<div class="text02">
					<p class="text_box_lead">【引受保険会社】損害保険ジャパン株式会社</p>
					<p>このご案内は概要を説明したものです。詳しい内容につきましては、パンフレットをご覧ください。なお、ご不明な点は、取扱代理店または損保ジャパンまでお問合せください。</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="sec03">

</section>
<section id="sec04">



</section>
<?php get_footer(); ?>