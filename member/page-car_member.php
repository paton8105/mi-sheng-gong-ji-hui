<?php
/*
Template Name: 自動車保険会員ページ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
	<section id="sec01">
		<div class="menber_band">自警会会員専用ページ</div>

		<div class="hero submember_bg">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">自動車保険</span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
				</div>
			</div>
		</div>
		<div class="breadcrumbs_wrap">
			<div class="contents_body_03">
				<div class="breadcrumbs submember_breadcrumbs">
					<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../">取扱保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list">自動車保険(会員ページ)</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec02" class="submember">
		<div class="contents_body_03">
			<h2 class="submember_ttl">自動車保険</h2>
			<div class="submemer_contents_box">
				<div class="submember_pdf">
					<figure><img src="<?php the_field('thum1'); ?>" alt="パンフレット"></figure>
					<figure><img src="<?php the_field('thum2'); ?>" alt="パンフレット"></figure>
				</div>
				<div class="text_wrap">
					<div class="text_box">
						<p class="text_box_ttl">自警会団体扱自動車保険の特徴</p>
						<ul>
							<li>
								<p>大口団体<span class="text_box_underline">割引35％が適用</span>されるためお得です。</p>
							</li>
							<li>
								<p>現在の<span class="text_box_underline">等級が継承</span>されます。（一部等級が継承できない共済がございます。詳しくは取扱代理店または保険会社までお問合せください。）</p>
							</li>
							<li>
								<p><span class="text_box_underline">ご家族のお車もご加入</span>いただけます。</p>
							</li>
						</ul>
					</div>
					<div class="car_text_wrap">
						<p class="car_text_ttl">ご連絡をいただく際には必ず以下の書類をご準備ください。</p>
						<ul class="car_text_list">
							<li class="car_text_list_item">
								<p class="car_list_item_ttl">初めて自動車保険への加入を検討される方</p>
								<ul class="list_mark_circle">
									<li>お車ご購入時の注文書</li>
									<li>売買契約書等（既にある場合は車検証）</li>
									<li>運転免許証</li>
								</ul>
							</li>
							<li class="car_text_list_item">
								<p class="car_list_item_ttl">他社の自動車保険からの切替を検討される方</p>
								<ul class="list_mark_circle">
									<li>ご契約中の保険証券</li>
									<li>継続案内等</li>
									<li>運転免許証</li>
								</ul>
							</li>
						</ul>
						<p class="car_text_ttl">※お見積もりには時間がかかります。余裕を持ってお早めにご連絡ください。</p>
					</div>
					<div class="download_wrap mt_s">
						<a href="<?php the_field('file'); ?>" target="_blank" class="download_link">
							<p class="download_link_pdf"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_pdf.svg" alt="pdf">リーフレット</p>
							<p class="download_link_download overpass">DOWNLOAD<img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_download_on.svg" alt="↓"></p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec03">

	</section>
	<section id="sec04">



	</section>
<?php get_footer(); ?>
