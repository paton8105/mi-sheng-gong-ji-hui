<?php
/*
Template Name: 自警会団体傷害 退職者延長保険会員ページ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<section id="sec01">
	<div class="menber_band">自警会会員専用ページ</div>
	<div class="hero submember_bg">
		<div class="page_body_01">
			<div class="page_ttl">
				<h1><span class="page_ttl_ja">自警会団体傷害<span class="ttl_sp_no">&emsp;</span><br class="ttl_pc_no"><span class="nowrap">退職者用延長保険</span></span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
			</div>
		</div>
	</div>
	<div class="breadcrumbs_wrap">
		<div class="contents_body_03">
			<div class="breadcrumbs submember_breadcrumbs">
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../../">取扱保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../">団体傷害・医療保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list">自警会団体傷害　退職者用延長保険</div>
			</div>
		</div>
	</div>
</section>
<section id="sec02" class="submember">
	<div class="contents_body_03">
		<h2 class="submember_ttl">自警会団体傷害　退職者用延長保険</h2>
		<div class="submemer_contents_box">
			<div class="submember_pdf">
				<figure><img src="<?php the_field('thum'); ?>" alt="パンフレット"></figure>
			</div>
			<div class="text_wrap">
				<div class="text_box">
					<p class="text_box_ttl">自警会団体傷害　退職者用延長保険の特徴</p>
					<ul>
						<li>
							<p>日本国内・国外を問わず、さまざまな傷害事故を補償します。</p>
						</li>
						<li>
							<p>R6年度より、加入対象者が広がります。<br>個人型の加入対象に会員と別居の配偶者の両親と子供の配偶者を追加。</p>
						</li>	
					</ul>
				</div>
				<div class="list_wrap">
					<ul class="numbering">
						<li class="numbering_list">
							<p class="list_ttl">大きな割引</p>
							<p>個人でご契約をする場合に比べ、約68%割引となっており大変お得です。</p>
						</li>
						<li class="numbering_list">
							<p class="list_ttl">幅広い補償</p>
							<ul class="list_mark_circle">
								<li>日本国内・国外を問わず、業務中から日常生活の災害、地震・噴火・津波などの自然災害によるケガを補償します。</li>
								<li>熱中症も補償します。</li>
								<li>入院・通院は１日目から補償します。</li>
							</ul>
						</li>
						<li class="numbering_list">
							<p class="list_ttl">更新のお手続き</p>
							<p>64才までは自動更新です。</p>
						</li>
					</ul>
				</div>
				<div class="download_wrap">
					<?php 
						$fields = CFS()->get('pamphlet'); 
						if (!empty($fields)) {
					?>
					<?php 
              foreach( $fields as $field ) {
                if (!empty($field['pamphlet_file'])) {
                  echo '<a href="'.$field['pamphlet_file'].'" target="_blank" class="download_link">
									<p class="download_link_pdf"><img src="'.get_template_directory_uri().'/images/common/ico_pdf.svg" alt="pdf">'.$field['pamphlet_ttl'].'</p>
									<p class="download_link_download overpass">DOWNLOAD<img src="'.get_template_directory_uri().'/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="'.get_template_directory_uri().'/images/common/ico_download_on.svg" alt="↓"></p>
								</a>';
                }
              }
            ?>
						  <?php 
								}
							?>
					</div>
			</div>
		</div>
	</div>
</section>
<section id="sec03">
</section>
<section id="sec04">
</section>
<?php get_footer(); ?>