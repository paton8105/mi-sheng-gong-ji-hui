<?php
/*
Template Name: 自警会介護サポート保険会員ページ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
	<section id="sec01">
		<div class="menber_band">自警会会員専用ページ</div>
		<div class="hero submember_bg">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">自警会介護サポート保険</span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
				</div>
			</div>
		</div>
		<div class="breadcrumbs_wrap">
			<div class="contents_body_03">
				<div class="breadcrumbs submember_breadcrumbs">
					<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../../">取扱保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../">団体傷害・医療保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list">自警会介護サポート保険</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec02" class="submember">
		<div class="contents_body_03">
			<h2 class="submember_ttl">自警会介護サポート保険</h2>
			<div class="submemer_contents_box">
				<div class="submember_pdf">
					<figure><img src="<?php the_field('thum1'); ?>" alt="パンフレット"></figure>
				</div>
				<div class="text_wrap">
					<p class="text_box_lead">ご本人とご両親に加え、配偶者と配偶者のご両親が要介護状態になった時に一時金をお支払いする保険です。</p>
					<div class="text_box">
						<p class="text_box_ttl">自警会介護サポート保険の特徴</p>
						<ul>
							<li>
								<p><span class="text_box_underline">団体割引20％適用</span>です。</p>
							</li>
							<li>
								<p>健康状態に関する<span class="text_box_underline">ご質問にご回答いただくだけで加入できます。</span></p>
							</li>	
							<li>
								<p>福祉用具の購入費・住宅の改修（廊下の手すり設置など）の初期費用のために<span class="text_box_underline">一時金をお支払い</span>します。</p>
							</li>	
						</ul>
					</div>
					<div class="download_wrap mt_s">
					<?php 
							$fields = CFS()->get('pamphlet'); 
							if (!empty($fields)) {
						?>
						<?php 
								foreach( $fields as $field ) {
									if (!empty($field['pamphlet_file'])) {
										echo '<a href="'.$field['pamphlet_file'].'" target="_blank" class="download_link">
										<p class="download_link_pdf"><img src="'.get_template_directory_uri().'/images/common/ico_pdf.svg" alt="pdf">'.$field['pamphlet_ttl'].'</p>
										<p class="download_link_download overpass">DOWNLOAD<img src="'.get_template_directory_uri().'/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="'.get_template_directory_uri().'/images/common/ico_download_on.svg" alt="↓"></p>
									</a>';
									}
								}
							?>
						  <?php 
								}
							?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec03" class="submember">
		<div class="contents_body_03">
			<h2 class="submember_ttl">退職者用 自警会介護サポート保険</h2>
			<div class="submemer_contents_box">
				<div class="submember_pdf">
					<figure><img src="<?php the_field('thum2'); ?>" alt="パンフレット"></figure>
				</div>
				<div class="text_wrap">
					<p class="text_box_lead">この制度は、在職中に「自警会介護サポート保険」にご加入されていた方が、退職後も継続加入いただける制度です。</p>
					<div class="text_box">
						<p class="text_box_ttl">退職者用 自警会介護サポート保険の特徴</p>
						<ul>
							<li>
								<p>所定の介護状態やケガによる死亡・後遺障害になった際に保険金をお支払いいたします。</p>
							</li>
							<li>
								<p><span class="text_box_underline">団体割引20％適用</span>します。</p>
							</li>
						</ul>
					</div>
					<div class="download_wrap mt_s">
						<?php 
							$fields = CFS()->get('pamphlet02'); 
							if (!empty($fields)) {
						?>
						<?php 
								foreach( $fields as $field ) {
									if (!empty($field['pamphlet_file02'])) {
										echo '<a href="'.$field['pamphlet_file02'].'" target="_blank" class="download_link">
										<p class="download_link_pdf"><img src="'.get_template_directory_uri().'/images/common/ico_pdf.svg" alt="pdf">'.$field['pamphlet_ttl02'].'</p>
										<p class="download_link_download overpass">DOWNLOAD<img src="'.get_template_directory_uri().'/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="'.get_template_directory_uri().'/images/common/ico_download_on.svg" alt="↓"></p>
									</a>';
									}
								}
							?>
						  <?php 
								}
							?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="sec04">



	</section>
<?php get_footer(); ?>
