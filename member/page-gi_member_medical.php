<?php
/*
Template Name: 自警会医療保険
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<section id="sec01">
	<div class="menber_band">自警会会員専用ページ</div>
	<div class="hero submember_bg">
		<div class="page_body_01">
			<div class="page_ttl">
				<h1><span class="page_ttl_ja">自警会医療保険</span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
			</div>
		</div>
	</div>
	<div class="breadcrumbs_wrap">
		<div class="contents_body_03">
			<div class="breadcrumbs submember_breadcrumbs">
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../../">取扱保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
					<div class="breadcrumbs_list"><a href="../">団体傷害・医療保険</a></div>
					<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list">自警会医療保険</div>
			</div>
		</div>
	</div>
</section>
<section id="sec02" class="submember">
	<div class="contents_body_03">
		<h2 class="submember_ttl">自警会医療保険<wbr><span class="nowrap">（医療保険基本特約</span><wbr><span class="nowrap">・疾病保険特約</span><wbr><span class="nowrap">・傷害保険特約</span><wbr><span class="nowrap">・がん保険特約セット<wbr><span class="nowrap">団体総合保険）</span></span></h2>
		<div class="submemer_contents_box">
			<div class="submember_pdf">
				<figure><img src="<?php the_field('thum'); ?>" alt="パンフレット"></figure>
			</div>
			<div class="text_wrap">
				<p class="text_box_lead">ご病気により継続して入院（５日以上）した場合の、退院後通院も補償します。</p>
				<div class="text_box">
					<p class="text_box_ttl">自警会医療保険の特徴</p>
					<ul>
						<li>
							<p>年齢・性別に関係なく<span class="text_box_underline">保険料は一律</span>です。</p>
						</li>
						<li>
							<p><span class="text_box_underline">割引33.5%が適用</span>されています。(団体割引30%、優良割引5%)</p>
						</li>
						<li>
							<p>『先進医療特約』の補償が<span class="text_box_underline">月々40円から加入</span>できます。</p>
						</li>
						<li>
							<p>がん保険特約は、<span class="text_box_underline">通院1日目から補償</span>します。（入院不要）</p>
						</li>
						<li>
							<p>R6年度より、抗がん剤治療補償プランが導入されました。<br>過去の損害率により、割引23%の適用となります。</p>
						</li>
					</ul>
				</div>
				<div class="download_wrap mt_s">
					<?php 
						$fields = CFS()->get('pamphlet'); 
						if (!empty($fields)) {
					?>
					<?php 
              foreach( $fields as $field ) {
                if (!empty($field['pamphlet_file'])) {
                  echo '<a href="'.$field['pamphlet_file'].'" target="_blank" class="download_link">
									<p class="download_link_pdf"><img src="'.get_template_directory_uri().'/images/common/ico_pdf.svg" alt="pdf">'.$field['pamphlet_ttl'].'</p>
									<p class="download_link_download overpass">DOWNLOAD<img src="'.get_template_directory_uri().'/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="'.get_template_directory_uri().'/images/common/ico_download_on.svg" alt="↓"></p>
								</a>';
                }
              }
            ?>
						  <?php 
								}
							?>
				</div>
				<div class="text02">
					<p class="text_box_lead">【引受保険会社】損害保険ジャパン株式会社</p>
					<p>このご案内は概要を説明したものです。詳しい内容につきましては、パンフレットをご覧ください。なお、ご不明な点は、取扱代理店または損保ジャパンまでお問合せください。</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="sec03">

</section>
<section id="sec04">



</section>
<?php get_footer(); ?>