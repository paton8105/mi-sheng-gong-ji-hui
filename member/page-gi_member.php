<?php
/*
Template Name: 団体傷害・医療保険会員ページ
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>
<section id="sec01" class="member">
  <div class="menber_band">自警会会員専用ページ</div>
  <div class="hero submember_bg">
    <div class="page_body_01">
      <div class="page_ttl">
        <h1><span class="page_ttl_ja">団体傷害・医療保険</span><span class="page_ttl_en overpass">FOR MEMBERS</span></h1>
      </div>
    </div>
  </div>
  <div class="breadcrumbs_wrap">
    <div class="contents_body_03">
      <div class="breadcrumbs submember_breadcrumbs">
        <div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
        <div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list"><a href="../">取扱保険</a></div>
        <div class="breadcrumbs_list arw_breadcrumbs">〉</div>
        <div class="breadcrumbs_list">団体傷害・医療保険</div>
      </div>
    </div>
  </div>
</section>
<section id="sec02">
  <div class="contents_body_03">
    <div class="inner_01">
      <h2 class="member_ttl_01">取扱種目</h2>
      <p class="member_ttl_02 overpass">LINEUP</p>
      <h3 class="member_ttl_03">自警会団体損害保険のご案内</h3>
      <p class="member_txt_01">弥生共済会では自警会の各種団体損害保険を取り扱っています。日常生活の様々なリスクからご家族の皆さまをお守りします！</p>
      <div class="member_list_sec">
        <div class="member_list_wrap">
          <h4 class="member_list_ttl">現職・再任用・会計年度任用職員・外部団体等の皆さま</h4>
          <div class="member_btn">
            <div class="download_wrap">
              <?php 
                $fields = CFS()->get('pamphlet'); 
                if (!empty($fields)) {
              ?>
              <?php 
                  foreach( $fields as $field ) {
                    if (!empty($field['pamphlet_file'])) {
                      echo '<a href="'.$field['pamphlet_file'].'" target="_blank" class="download_link">
                      <p class="download_link_pdf"><img src="'.get_template_directory_uri().'/images/common/ico_pdf.svg" alt="pdf">'.$field['pamphlet_ttl'].'</p>
                      <p class="download_link_download overpass">DOWNLOAD<img src="'.get_template_directory_uri().'/images/common/ico_download.svg" alt="↓" class="download_link_download_icon_on"><img src="'.get_template_directory_uri().'/images/common/ico_download_on.svg" alt="↓"></p>
                    </a>';
                    }
                  }
                ?>
              <?php 
                }
              ?>
            </div>
          </div>
          <ul class="member_list">
            <li class="member_listitem">
              <a href="<?php echo home_url(); ?>/member/insurance/group/injury/" class="member_card">
                <figure class="member_img">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/group-insurance_member/img_01_pc.jpg" alt="自警会団体傷害保険" class="image-switch">
                </figure>
                <div class="member_txtbox">
                  <p class="member_txt_02">自警会団体傷害保険<br>長期給与サポートプラン</p>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_nav_list.svg" alt="右矢印" class="arw_r">
                </div>
              </a>
            </li>
            <li class="member_listitem">
              <a href="<?php echo home_url(); ?>/member/insurance/group/medical/" class="member_card">
                <figure class="member_img">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/group-insurance_member/img_03_pc.jpg" alt="自警会医療保険" class="image-switch">
                </figure>
                <div class="member_txtbox">
                  <p class="member_txt_02">自警会医療保険<br>がん保険特約</p>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_nav_list.svg" alt="右矢印" class="arw_r">
                </div>
              </a>
            </li>
            <li class="member_listitem">
              <a href="<?php echo home_url(); ?>/member/insurance/group/nursing/" class="member_card">
                <figure class="member_img">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/group-insurance_member/img_05_pc.jpg" alt="介護サポート保険" class="image-switch">
                </figure>
                <div class="member_txtbox">
                  <p class="member_txt_02">介護サポート保険</p>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_nav_list.svg" alt="右矢印" class="arw_r">
                </div>
              </a>
            </li>
          </ul>
        </div>
        <div class="member_list_wrap member_list_wrap_02">
          <h4 class="member_list_ttl">OBの皆さま</h4>
          <ul class="member_list">
            <li class="member_listitem">
              <a href="<?php echo home_url(); ?>/member/insurance/group/retiree-injury/" class="member_card">
                <figure class="member_img">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/group-insurance_member/img_06_pc.jpg" alt="自警会団体傷害保険" class="image-switch">
                </figure>
                <div class="member_txtbox">
                  <p class="member_txt_02">自警会団体傷害保険<br>退職者用延長保険</p>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_nav_list.svg" alt="右矢印" class="arw_r">
                </div>
              </a>
            </li>
            <li class="member_listitem">
              <a href="<?php echo home_url(); ?>/member/insurance/group/retiree/" class="member_card">
                <figure class="member_img">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/group-insurance_member/img_04_pc.jpg" alt="自警会退職者総合補償制度" class="image-switch">
                </figure>
                <div class="member_txtbox">
                  <p class="member_txt_02">自警会退職者総合補償制度</p>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/common/arw_nav_list.svg" alt="右矢印" class="arw_r">
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="sec03">

</section>
<section id="sec04">

</section>
<?php get_footer(); ?>