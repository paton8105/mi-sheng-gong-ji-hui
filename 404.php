<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<section id="sec01">
	<div class="hero">
		<div class="pp_page_ttl_wrap">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">お探しのページは見つかりません</span><span class="pp_page_ttl_en overpass">Not Found</span></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumbs_wrap">
		<div class="contents_body_03">
			<div class="breadcrumbs submember_breadcrumbs">
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
				<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list">お探しのページは見つかりません</div>
			</div>
		</div>
	</div>
</section>
<section id="sec02" class=" ">
	<div class="contents_body_03 unknown_wrap ">
		<div class="inner_01 submember">
			<div class="text_box">
				<p>アクセスしようとしたページは削除されたかURLが変更されているため、見つけることができません。</p>
			</div>
			<div class="foot_contact_bnr">
					<a href="<?php echo home_url(''); ?>/">トップページへ<img src="<?php echo get_template_directory_uri(); ?>/images/common/footer_contact_arw.svg" alt="右矢印" class="arw_icon"></a>
				</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
