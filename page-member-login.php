<?php
/*
Template Name: 会員専用ログイン
*/
?>
<?php get_template_part('/include/head-meta'); ?>
<?php get_template_part('/include/head-link'); ?>
<?php get_header(); ?>

<section id="sec01">
	<div class="hero">
		<div class="pp_page_ttl_wrap">
			<div class="page_body_01">
				<div class="page_ttl">
					<h1><span class="page_ttl_ja">会員専用ログイン</span><span class="pp_page_ttl_en overpass">MEMBER LOGIN</span></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcrumbs_wrap">
		<div class="contents_body_03">
			<div class="breadcrumbs">
				<div class="breadcrumbs_list"><a href="<?php echo home_url(); ?>">HOME</a></div>
				<div class="breadcrumbs_list arw_breadcrumbs">〉</div>
				<div class="breadcrumbs_list">会員専用ログイン</div>
			</div>
		</div>
	</div>
</section>
<section id="sec02" class="mlogbg login_mtop">
	<div class="contents_body_03">
		<div class="inner_01">
			<p class="login_info">このコンテンツはパスワードで保護されています。<br>閲覧するには以下にパスワードを入力してください。</p>
			<form name="mlogin" method="post" enctype="multipart/form-data" action="<?php echo esc_url( home_url( '/member/' ) ); ?>">
				<div class="passbox">
					<label class="pass_lb">パスワード</label>
					<input class="pass_input" type="password" name="pass" size="40" maxlength="200" value="<?php echo ($pass)?$pass:"";?>" required />
				</div>
				<p class="error"><?php echo $msg ?></p>
				<div class="login_submit">
					<div class="login_submit_btdeco"><input class="login_submit_bt" type="submit" value="ログイン" /><img src="<?php echo get_template_directory_uri(); ?>/images/top/more_btn.svg" alt="右矢印" class="login_submit_btarw"></div>
					<input type="hidden" name="action" value="loginchk">
				</div>
			</form>
			<a href="<?php echo home_url(); ?>/forgot/" class="pass_unknown">パスワードが不明な方はこちらをご覧ください</a>
		</div>
	</div>
</section>
<?php get_footer(); ?>
